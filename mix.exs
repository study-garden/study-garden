defmodule StudyGarden.MixProject do
  use Mix.Project

  def project do
    [
      app: :study_garden,
      version: "0.1.0",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {StudyGarden.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:argon2_elixir, "~> 3.0"},
      {:phoenix, "~> 1.7.11"},
      {:phoenix_ecto, "~> 4.4"},
      {:ecto_sql, "~> 3.10"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 4.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.20.2"},
      {:floki, ">= 0.30.0"},
      {:phoenix_live_dashboard, "~> 0.8.3"},
      {:esbuild, "~> 0.8", runtime: Mix.env() == :dev},
      {:tailwind, "~> 0.2", runtime: Mix.env() == :dev},
      {
        :heroicons,
        # override needed over phoenix_storybook v0.6.0
        github: "tailwindlabs/heroicons",
        tag: "v2.1.1",
        sparse: "optimized",
        app: false,
        compile: false,
        depth: 1,
        override: true
      },
      {:swoosh, "~> 1.5"},
      {:finch, "~> 0.13"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.20"},
      {:jason, "~> 1.2"},
      {:dns_cluster, "~> 0.1.1"},
      {:bandit, "~> 1.2"},
      {:sobelow, "~> 0.13", only: [:dev, :test], runtime: false},
      {:mix_audit, "~> 2.1", only: [:dev, :test], runtime: false},
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:phoenix_storybook, "~> 0.6.0"},
      {:oban, "~> 2.17"},
      {:ex_cldr, "~> 2.3"},
      {:ex_cldr_numbers, "~> 2.3"},
      {:ex_cldr_dates_times, "~> 2.0"},
      {:cldr_html,
       git: "https://github.com/study-garden/cldr_html.git", branch: "add-phoenix-html-v4-support"},
      {:ex_cldr_routes, "~> 1.2"},
      {:ex_money, "~> 5.0"},
      {:ex_money_sql, "~> 1.0"},
      {:nostrum, "~> 0.8"},
      {:ecto_psql_extras, "~> 0.6"},
      {:cloak_ecto, "~> 1.2.0"},
      {:ueberauth_discord,
       git: "https://gitlab.com/study-garden/ueberauth/ueberauth_discord.git", tag: "v0.8.0"},
      {:stripity_stripe, "~> 3.1"},
      {:timex, "~> 3.0"},
      {:sentry, "~> 10.2.0"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "assets.setup", "assets.build"],
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "ecto.seed": ["run priv/repo/seeds.exs"],
      "ecto.seed_mimbres": ["run priv/repo/mimbres_seeds.exs"],
      test: ["ecto.reset --quiet", "ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "assets.setup": ["tailwind.install --if-missing", "esbuild.install --if-missing"],
      "assets.build": ["tailwind study_garden", "esbuild study_garden"],
      "assets.deploy": [
        "phx.digest",
        "esbuild study_garden --minify",
        "tailwind study_garden --minify",
        "tailwind storybook --minify"
      ]
    ]
  end
end

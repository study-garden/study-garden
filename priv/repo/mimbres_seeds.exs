# Mimbres seed data

alias StudyGarden.Repo
alias StudyGarden.Schools.{School, Announcement}
alias StudyGarden.Courses.{Course, CourseSession}

# School details
Repo.insert!(%School{
  full_name: "Mimbres School for the Humanities",
  short_name: "Mimbres",
  description: """
  Mimbres School for the Humanities is an intellectual community and interdisciplinary school for the humanities, located online and in the Mimbres Valley of southern New Mexico, organized by Colin Drumm, Jules Delisle, and Alirio Karina. The School offers online courses for an affordable cost, as well as a large and growing archive of previous courses available to subscribers.\n
  The school’s curriculum is premised on a skepticism towards the established boundaries of academic disciplines, and seeks to provide students with the grounding and tools necessary for a critical exploration of the problems, contradictions, and antagonisms that structure the world they find themselves to have inherited. The Mimbres School values in its students and scholars an attention to the concrete and empirical, a willingness to be surprised by phenomena, and an impiety toward canonical texts and received ideas.
  """,
  timezone: "America/Phoenix",
  google_calendar_embed_url:
    "<iframe src=\"https://calendar.google.com/calendar/embed?height=600&wkst=1&ctz=America%2FPhoenix&bgcolor=%2333B679&src=dGhlbWltYnJlc3NjaG9vbEBnbWFpbC5jb20&color=%23039BE5\" style=\"border:solid 1px #777\" width=\"800\" height=\"600\" frameborder=\"0\" scrolling=\"no\"></iframe>"
})

# 2024 summer courses
Repo.insert!(%Course{
  title: "Dealing with Graeber",
  description:
    "A critical appraisal of the major works of David Graeber, who rose to fame as the theorist of the \"Occupy Wall St\" movement and left behind an influential scholarly and popular legacy in the form of a grand synthetic re-evaluation of received wisdom about the history of human civilization. At the core of Graeber's wide-ranging work is an optimism of the will that always attempts to demonstrate the contingency of what might otherwise be imagined as necessary, and thus immutable: to show that, however things are, they don't have to be that way, and can indeed be better. The course will attempt to strike a balance between appreciation for Graeber's willingness to table \"big questions\" and a skepticism towards his polemical framing.",
  start_date: ~D[2024-06-01],
  end_date: ~D[2024-08-24],
  tuition: Money.new(:USD, 250),
  deposit: Money.new(:USD, 100),
  slug: "graeber",
  stripe_tuition_price_id: "price_1PAHGIJq9VrZV4rL6ttCmInv",
  stripe_deposit_price_id: "price_1PAHGIJq9VrZV4rLTw2ka3zs",
  sessions: [
    %CourseSession{
      start: ~U[2024-06-01 05:00:00Z],
      end: ~U[2024-06-01 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-06-15 05:00:00Z],
      end: ~U[2024-06-15 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-06-29 05:00:00Z],
      end: ~U[2024-06-29 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-07-13 05:00:00Z],
      end: ~U[2024-07-13 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-07-27 05:00:00Z],
      end: ~U[2024-07-27 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-08-10 05:00:00Z],
      end: ~U[2024-08-10 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-08-24 05:00:00Z],
      end: ~U[2024-08-24 07:00:00Z]
    }
  ]
})

Repo.insert!(%Course{
  title: "Gold, the Barbarous Relic",
  description:
    "Gold does not tarnish. We craft it, crave it, hoard it, loot it, die for it, dig it up and bury it in the ground. ‘Gold the Barbarous Relic’ is a six-session course on the metal that has captivated human societies and examines mass migrations, political transformations, environmental calamities and new political economies precipitated by the insatiable lust for gold from antiquity to the present. What is it about gold that captivates us? And at what cost has the lust for gold been pursued?",
  start_date: ~D[2024-06-08],
  end_date: ~D[2024-08-17],
  tuition: Money.new(:USD, 250),
  deposit: Money.new(:USD, 100),
  slug: "gold-the-barbarous-relic",
  stripe_tuition_price_id: "price_1PAHHJJq9VrZV4rLSR3U3P0h",
  stripe_deposit_price_id: "price_1PAHIfJq9VrZV4rLm6u1c86A",
  sessions: [
    %CourseSession{
      start: ~U[2024-06-08 05:00:00Z],
      end: ~U[2024-06-08 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-06-22 05:00:00Z],
      end: ~U[2024-06-22 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-07-06 05:00:00Z],
      end: ~U[2024-07-06 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-07-20 05:00:00Z],
      end: ~U[2024-07-20 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-08-03 05:00:00Z],
      end: ~U[2024-08-03 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-08-17 05:00:00Z],
      end: ~U[2024-08-17 07:00:00Z]
    }
  ]
})

Repo.insert!(%Course{
  title: "Black Reconstruction",
  description:
    "W. E. B. Du Bois’s Black Reconstruction challenged the dominant historiographical view both on its merits, and as a matter of its position. The histories preceding it sought to produce a legitimacy myth for the planter order and its dynastic descendants, playing to white fears of subordination to African Americans. They understood the racial violence of the era as the natural consequence of disordering the correct order of racial position, and the racial controls that succeeded it as a necessary return. Du Bois’s Marxian corrective instead reckoned with Reconstruction as a period in which legal and democratic possibility flourished but was radically curtailed by whiteness’s wage. Over the course of seven weeks, we will closely read Black Reconstruction, examining the fraught entanglement of law, authority and legitimacy with the promise of redistribution and the perpetuation of race and dynasty.",
  start_date: ~D[2024-06-01],
  end_date: ~D[2024-08-24],
  tuition: Money.new(:USD, 250),
  deposit: Money.new(:USD, 100),
  slug: "black-reconstruction",
  stripe_tuition_price_id: "price_1PAHGrJq9VrZV4rL1GxcBiDS",
  stripe_deposit_price_id: "price_1PAHGrJq9VrZV4rL8Tqy1GcM",
  sessions: [
    %CourseSession{
      start: ~U[2024-06-09 05:00:00Z],
      end: ~U[2024-06-09 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-06-23 05:00:00Z],
      end: ~U[2024-06-23 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-07-07 05:00:00Z],
      end: ~U[2024-07-07 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-07-21 05:00:00Z],
      end: ~U[2024-07-21 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-08-04 05:00:00Z],
      end: ~U[2024-08-04 07:00:00Z]
    },
    %CourseSession{
      start: ~U[2024-08-18 05:00:00Z],
      end: ~U[2024-08-18 07:00:00Z]
    }
  ]
})

# School announcement
Repo.insert!(%Announcement{
  message:
    "Summer course enrollments are now open! Click <a class=\"tw-font-bold hover:tw-underline\" href=\"/courses/\">here</a> to enroll."
})

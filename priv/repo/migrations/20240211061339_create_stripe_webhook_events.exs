defmodule StudyGarden.Repo.Migrations.CreateStripeWebhookEvents do
  use Ecto.Migration

  def change do
    create table(:stripe_webhook_events, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :stripe_id, :string, null: false
      add :stripe_type, :string, null: false
      add :host, :string, null: false
      add :stripe_created, :integer, null: false
      add :stripe_api_version, :string, null: false
      add :stripe_idempotency_key, :string
      add :stripe_signature, :string, null: false
      add :stripe_request_id, :string

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:stripe_webhook_events, [:stripe_id])
  end
end

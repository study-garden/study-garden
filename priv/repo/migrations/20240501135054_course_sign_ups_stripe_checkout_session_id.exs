defmodule StudyGarden.Repo.Migrations.CourseSignUpsStripeCheckoutSessionId do
  use Ecto.Migration

  def change do
    alter table(:course_sign_ups) do
      add :stripe_checkout_session_id, :string
    end
  end
end

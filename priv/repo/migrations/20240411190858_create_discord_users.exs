defmodule StudyGarden.Repo.Migrations.CreateDiscordUsers do
  use Ecto.Migration

  def change do
    create table(:discord_users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :discord_id, :string, null: false
      add :discord_username, :string, null: false
      add :discord_email, :string
      add :discord_verified, :boolean, default: false, null: false, null: false
      add :access_token, :binary
      add :access_token_expires_at, :integer
      add :refresh_token, :binary
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id), null: false

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:discord_users, [:discord_id])
    create index(:discord_users, [:user_id])
  end
end

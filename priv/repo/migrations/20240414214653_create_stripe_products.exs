defmodule StudyGarden.Repo.Migrations.CreateStripeProducts do
  use Ecto.Migration

  def change do
    create table(:stripe_products, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :stripe_id, :string
      add :stripe_name, :string
      add :stripe_active, :boolean, default: false, null: false
      add :default_price, :string
      add :stripe_metadata, :map
      add :stripe_created, :integer
      add :stripe_updated, :integer
      add :stripe_url, :string

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:stripe_products, [:stripe_id])
  end
end

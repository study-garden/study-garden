defmodule StudyGarden.Repo.Migrations.CreateSchools do
  use Ecto.Migration

  def change do
    create table(:schools, primary_key: false) do
      add :id, :boolean, primary_key: true, default: true
      add :full_name, :string
      add :short_name, :string
      add :description, :text
      add :timezone, :string
      add :stripe_customer_portal_url, :string
      add :google_calendar_embed_url, :text

      timestamps(type: :utc_datetime_usec)
    end

    create constraint(:schools, :only_one_school, check: "id")
    create index(:schools, [:short_name])
  end
end

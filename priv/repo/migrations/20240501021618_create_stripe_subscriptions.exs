defmodule StudyGarden.Repo.Migrations.CreateStripeSubscriptions do
  use Ecto.Migration

  def change do
    create table(:stripe_subscriptions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :stripe_id, :string
      add :stripe_cancel_at_period_end, :boolean, default: false, null: false
      add :stripe_current_period_start, :integer
      add :stripe_current_period_end, :integer
      add :stripe_ended_at, :integer
      add :stripe_customer, :string
      add :stripe_items, :map
      add :stripe_latest_invoice, :string
      add :stripe_metadata, :map
      add :stripe_status, :string
      add :stripe_cancel_at, :integer
      add :stripe_cancelled_at, :integer
      add :stripe_created, :integer
      add :stripe_days_until_due, :integer
      add :stripe_start_date, :integer
      add :sg_stripe_key, :binary_id

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:stripe_subscriptions, [:stripe_id])
  end
end

defmodule StudyGarden.Repo.Migrations.CreateStripeCustomers do
  use Ecto.Migration

  def change do
    create table(:stripe_customers, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :stripe_id, :string, null: false
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:stripe_customers, [:stripe_id])
    create index(:stripe_customers, [:user_id])
  end
end

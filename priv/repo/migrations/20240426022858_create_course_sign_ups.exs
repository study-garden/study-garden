defmodule StudyGarden.Repo.Migrations.CreateCourseSignUps do
  use Ecto.Migration

  def change do
    create table(:course_sign_ups, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :email, :string
      add :discord_username, :string
      add :payment_option, :string
      add :course_id, references(:courses, on_delete: :nothing, type: :binary_id)

      timestamps(type: :utc_datetime_usec)
    end

    create index(:course_sign_ups, [:course_id])
  end
end

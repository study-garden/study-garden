defmodule StudyGarden.Repo.Migrations.CreateDiscordGuildRoles do
  use Ecto.Migration

  def change do
    create table(:discord_guild_roles, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :discord_id, :string
      add :discord_name, :string
      add :discord_position, :integer
      add :discord_color, :integer
      add :discord_managed, :boolean, default: false, null: false

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:discord_guild_roles, [:discord_id])
  end
end

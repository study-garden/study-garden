defmodule StudyGarden.Repo.Migrations.CourseSignUpOptionalComment do
  use Ecto.Migration

  def change do
    alter table(:course_sign_ups) do
      add :comment, :text
    end

  end
end

defmodule StudyGarden.Repo.Migrations.CreateCourses do
  use Ecto.Migration

  def change do
    create table(:courses, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :title, :string, null: false
      add :subtitle, :string
      add :description, :text
      add :start_date, :date, null: false
      add :end_date, :date
      add :tuition, :money_with_currency, null: false
      add :deposit, :money_with_currency
      add :slug, :string, null: false

      timestamps(type: :utc_datetime_usec)
    end

    create unique_index(:courses, [:slug])
  end
end

defmodule StudyGarden.Repo.Migrations.RenameStripeSubscriptionCancelledAt do
  use Ecto.Migration

  def change do
    rename table(:stripe_subscriptions), :stripe_cancelled_at, to: :stripe_canceled_at
  end
end

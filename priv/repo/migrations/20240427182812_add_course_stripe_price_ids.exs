defmodule StudyGarden.Repo.Migrations.AddCourseStripePriceIds do
  use Ecto.Migration

  def change do
    alter table(:courses) do
      add :stripe_tuition_price_id, :string
      add :stripe_deposit_price_id, :string
    end
  end
end

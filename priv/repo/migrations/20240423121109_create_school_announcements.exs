defmodule StudyGarden.Repo.Migrations.CreateSchoolAnnouncements do
  use Ecto.Migration

  def change do
    create table(:school_announcements, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :pinned, :boolean, default: false, null: false
      add :message, :text

      timestamps(type: :utc_datetime_usec)
    end
  end
end

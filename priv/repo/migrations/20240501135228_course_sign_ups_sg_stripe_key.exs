defmodule StudyGarden.Repo.Migrations.CourseSignUpsSgStripeKey do
  use Ecto.Migration

  def change do
    alter table(:course_sign_ups) do
      add :sg_stripe_key, :binary_id
    end
  end
end

defmodule StudyGarden.Repo.Migrations.CreateCourseSessions do
  use Ecto.Migration

  def change do
    create table(:course_sessions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :start, :utc_datetime
      add :end, :utc_datetime
      add :zoom_url, :string
      add :course_id, references(:courses, on_delete: :nothing, type: :binary_id)

      timestamps(type: :utc_datetime_usec)
    end

    create index(:course_sessions, [:course_id])
  end
end

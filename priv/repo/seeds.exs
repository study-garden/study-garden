# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     StudyGarden.Repo.insert!(%StudyGarden.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias StudyGarden.Repo
alias StudyGarden.Courses.{Course, CourseSession}

# Mimbres summer term courses
Repo.insert!(
  %Course{
    title: "Graeber",
    start_date: ~D[2024-06-01],
    end_date: ~D[2024-08-24],
    tuition: Money.new(:USD, 250),
    deposit: Money.new(:USD, 100),
    slug: "graeber",
    sessions: [
        %CourseSession{
          start: ~U[2024-06-01 00:06:00Z],
          end: ~U[2024-06-01 00:09:00Z],
        },
        %CourseSession{
          start: ~U[2024-06-15 00:06:00Z],
          end: ~U[2024-06-15 00:09:00Z],
        },
        %CourseSession{
          start: ~U[2024-06-29 00:06:00Z],
          end: ~U[2024-06-29 00:09:00Z],
        },
        %CourseSession{
          start: ~U[2024-07-13 00:06:00Z],
          end: ~U[2024-07-13 00:09:00Z],
        },
        %CourseSession{
          start: ~U[2024-07-27 00:06:00Z],
          end: ~U[2024-07-27 00:09:00Z],
        },
        %CourseSession{
          start: ~U[2024-08-10 00:06:00Z],
          end: ~U[2024-08-10 00:09:00Z],
        },
        %CourseSession{
          start: ~U[2024-08-24 00:06:00Z],
          end: ~U[2024-08-24 00:09:00Z],
        }
    ]
  }
)

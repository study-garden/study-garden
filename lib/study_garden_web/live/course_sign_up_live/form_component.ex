defmodule StudyGardenWeb.CourseSignUpLive.FormComponent do
  use StudyGardenWeb, :live_component
  use Phoenix.Component

  alias StudyGarden.Courses

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Sign up for the <%= @course.title %> course.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="course_sign_up-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input
          field={@form[:email]}
          type="text"
          label="Email"
          help_text="If your email differs from the one used to create your Discord account, you will need to verify it after signing up."
        />
        <.input
          field={@form[:discord_username]}
          type="text"
          label="Discord username"
          help_text="Make sure this is your Discord username and not your server nickname."
        />

        <span class="tw-block tw-text-sm tw-leading-6 tw-text-zinc-600">
          Don't have a Discord account? Register
          <a
            class="tw-text-sm tw-text-brand tw-font-bold hover:tw-underline"
            href="https://discord.com/register"
          >
            here.
          </a>
        </span>

        <.payment_options_radio_fieldset
          field={@form[:payment_option]}
          options={Ecto.Enum.dump_values(StudyGarden.Courses.CourseSignUp, :payment_option)}
          checked_value={@form.params["payment_option"]}
          course={@course}
        />

        <.input
          field={@form[:course_id]}
          class="phx-no-feedback:hidden"
          type="hidden"
          name="course_id"
          value={@course.id}
        />

        <:actions>
          <.button phx-disable-with="Saving...">Sign Up</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{course_sign_up: course_sign_up} = assigns, socket) do
    changeset = Courses.change_course_sign_up(course_sign_up)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"course_sign_up" => course_sign_up_params}, socket) do
    changeset =
      socket.assigns.course_sign_up
      |> Map.put(:course_id, socket.assigns.course.id)
      |> Courses.change_course_sign_up(course_sign_up_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"course_sign_up" => course_sign_up_params}, socket) do
    save_course_sign_up(socket, socket.assigns.action, course_sign_up_params)
  end

  defp save_course_sign_up(socket, :new, course_sign_up_params) do
    # Create a Stripe checkout session for course
    endpoint_url = StudyGardenWeb.Endpoint.url()
    course_slug = socket.assigns.course.slug

    price =
      case course_sign_up_params["payment_option"] do
        "full" -> socket.assigns.course.stripe_tuition_price_id
        "deposit" -> socket.assigns.course.stripe_deposit_price_id
        nil -> ""
      end

    # Study Garden to Stripe key
    sg_stripe_key = Ecto.UUID.generate()

    # Create Stripe checkout session
    stripe_checkout_session_params = %{
      line_items: [
        %{
          price: price,
          quantity: 1
        }
      ],
      mode: "payment",
      success_url:
        endpoint_url <> "/courses/#{course_slug}/sign-up/success?session_id={CHECKOUT_SESSION_ID}",
      cancel_url: endpoint_url <> "/courses/#{course_slug}/sign-up/cancelled",
      automatic_tax: %{enabled: true},
      payment_intent_data: %{
        metadata: %{"sg_product_type" => "course", "sg_stripe_key" => sg_stripe_key}
      },
      customer_email: course_sign_up_params["email"],
      invoice_creation: %{enabled: true}
    }

    course_sign_up_params =
      course_sign_up_params |> Map.put("course_id", socket.assigns.course.id)

    course_sign_up_params =
      course_sign_up_params |> Map.put("sg_stripe_key", sg_stripe_key)

    with {:ok, stripe_checkout_session} <-
           Stripe.Checkout.Session.create(stripe_checkout_session_params) do
      course_sign_up_params =
        course_sign_up_params |> Map.put("stripe_checkout_session_id", stripe_checkout_session.id)

      {:ok, course_sign_up} = Courses.create_course_sign_up(course_sign_up_params)

      notify_parent({:saved, course_sign_up})

      {:noreply,
       socket
       |> redirect(external: stripe_checkout_session.url)}
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}

      {:error, %Stripe.ApiErrors{} = _stripe_api_error} ->
        {:noreply,
         socket
         |> put_flash(:error, "Something went wrong processing Stripe payment")
         |> push_patch(to: socket.assigns.patch)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end

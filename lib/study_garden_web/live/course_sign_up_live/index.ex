defmodule StudyGardenWeb.CourseSignUpLive.Index do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Courses
  alias StudyGarden.Courses.CourseSignUp

  @impl true
  def mount(%{"course_id" => course_id}, _session, socket) do
    course = Courses.get_course!(course_id)
    socket = socket |> assign(:course, course)
    {:ok, stream(socket, :course_sign_ups, Courses.list_course_sign_ups())}
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :course_sign_ups, Courses.list_course_sign_ups())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Course sign up")
    |> assign(:course_sign_up, Courses.get_course_sign_up!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Course sign up")
    |> assign(:course_sign_up, %CourseSignUp{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Course sign ups")
    |> assign(:course_sign_up, nil)
  end

  @impl true
  def handle_info(
        {StudyGardenWeb.CourseSignUpLive.FormComponent, {:saved, course_sign_up}},
        socket
      ) do
    {:noreply, stream_insert(socket, :course_sign_ups, course_sign_up)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    course_sign_up = Courses.get_course_sign_up!(id)
    {:ok, _} = Courses.delete_course_sign_up(course_sign_up)

    {:noreply, stream_delete(socket, :course_sign_ups, course_sign_up)}
  end
end

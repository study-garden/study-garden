defmodule StudyGardenWeb.CourseSignUpLive.SignUp do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Courses
  alias StudyGarden.Courses.CourseSignUp

  @impl true
  def mount(%{"course_slug" => course_slug}, _session, socket) do
    course = Courses.get_course_by_slug!(course_slug)
    socket = socket |> assign(course: course)
    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Course sign up")
    |> assign(:course_sign_up, Courses.get_course_sign_up!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "#{socket.assigns.course.title} Sign Up")
    |> assign(:course_sign_up, %CourseSignUp{})
  end

  defp apply_action(socket, :confirm, _params) do
    socket
    |> assign(:page_title, "Confirm Sign Up")
  end

  @impl true
  def handle_info(
        {StudyGardenWeb.CourseSignUpLive.FormComponent, {:saved, course_sign_up}},
        socket
      ) do
    {:noreply, stream_insert(socket, :course_sign_ups, course_sign_up)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    course_sign_up = Courses.get_course_sign_up!(id)
    {:ok, _} = Courses.delete_course_sign_up(course_sign_up)

    {:noreply, stream_delete(socket, :course_sign_ups, course_sign_up)}
  end
end

defmodule StudyGardenWeb.CourseSignUpLive.SignUpPostCheckout do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Schools
  alias StudyGarden.Courses
  alias StudyGarden.Courses.CourseSignUp

  @impl true
  def mount(%{"session_id" => session_id, "course_slug" => course_slug}, _session, socket) do
    school = Schools.get_school!()

    socket =
      socket
      |> assign(:session_id, session_id)
      |> assign(:school, school)

    {:ok, checkout_session} =
      Stripe.Checkout.Session.retrieve(socket.assigns.session_id)

    {:ok, payment_intent} =
      Stripe.PaymentIntent.retrieve(checkout_session.payment_intent)

    course_sign_up =
      Courses.get_course_sign_up_by_stripe_checkout_session_id!(checkout_session.id)

    course =
      Courses.get_course_by_slug!(course_slug)

    case course do
      course ->
        socket =
          socket
          |> assign(:checkout_session, checkout_session)
          |> assign(:payment_intent, payment_intent)
          |> assign(:course, course)
          |> assign(:course_sign_up, course_sign_up)

        {:ok, socket}

      _ ->
        socket
        |> put_flash(:error, "Course sign up not found.")
        |> redirect(to: ~p"/")
    end
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  defp apply_action(socket, :success, _params) do
    socket
    |> assign(:page_title, "Course Sign Up Success!")
  end

  defp apply_action(socket, :cancelled, _params) do
    socket
  end
end

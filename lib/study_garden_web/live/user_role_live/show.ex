defmodule StudyGardenWeb.UserRoleLive.Show do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Access

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:user_role, Access.get_user_role!(id))}
  end

  defp page_title(:show), do: "Show User role"
end

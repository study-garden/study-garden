defmodule StudyGardenWeb.UserRoleLive.Index do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Identities
  alias StudyGarden.Access
  alias StudyGarden.Access.UserRole

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :user_roles, Access.list_user_roles())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :new, _params) do
    users = Identities.list_users()
    roles = Access.list_roles()

    socket
    |> assign(:users, users)
    |> assign(:roles, roles)
    |> assign(:page_title, "New User role")
    |> assign(:user_role, %UserRole{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing User roles")
    |> assign(:user_role, nil)
  end

  @impl true
  def handle_info({StudyGardenWeb.UserRoleLive.FormComponent, {:saved, user_role}}, socket) do
    {:noreply, stream_insert(socket, :user_roles, user_role)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    user_role = Access.get_user_role!(id)
    {:ok, _} = Access.delete_user_role(user_role)

    {:noreply, stream_delete(socket, :user_roles, user_role)}
  end
end

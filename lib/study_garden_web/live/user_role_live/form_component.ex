defmodule StudyGardenWeb.UserRoleLive.FormComponent do
  use StudyGardenWeb, :live_component

  alias StudyGarden.Access

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage user_role records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="user_role-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input
          field={@form[:user_id]}
          type="select"
          label="User"
          options={Enum.map(@users, fn user -> {user.email, user.id} end)}
        />
        <.input
          field={@form[:role_id]}
          type="select"
          label="Role"
          options={Enum.map(@roles, fn role -> {role.name, role.id} end)}
        />
        <:actions>
          <.button phx-disable-with="Saving...">Save User role</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{user_role: user_role} = assigns, socket) do
    changeset = Access.change_user_role(user_role)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"user_role" => user_role_params}, socket) do
    changeset =
      socket.assigns.user_role
      |> Access.change_user_role(user_role_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"user_role" => user_role_params}, socket) do
    save_user_role(socket, socket.assigns.action, user_role_params)
  end

  defp save_user_role(socket, :new, user_role_params) do
    case Access.create_user_role(user_role_params) do
      {:ok, user_role} ->
        notify_parent({:saved, user_role})

        {:noreply,
         socket
         |> put_flash(:info, "User role created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end

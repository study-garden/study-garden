defmodule StudyGardenWeb.CourseSessionLive.FormComponent do
  use StudyGardenWeb, :live_component

  alias StudyGarden.Courses

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage course_session records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="course_session-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:start]} type="datetime-local" label="Start" />
        <.input field={@form[:end]} type="datetime-local" label="End" />
        <.input field={@form[:zoom_url]} type="text" label="Zoom url" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Course session</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{course_session: course_session} = assigns, socket) do
    changeset = Courses.change_course_session(course_session)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"course_session" => course_session_params}, socket) do
    changeset =
      socket.assigns.course_session
      |> Courses.change_course_session(course_session_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"course_session" => course_session_params}, socket) do
    save_course_session(socket, socket.assigns.action, course_session_params)
  end

  defp save_course_session(socket, :edit, course_session_params) do
    case Courses.update_course_session(socket.assigns.course_session, course_session_params) do
      {:ok, course_session} ->
        notify_parent({:saved, course_session})

        {:noreply,
         socket
         |> put_flash(:info, "Course session updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_course_session(socket, :new, course_session_params) do
    case Courses.create_course_session(course_session_params) do
      {:ok, course_session} ->
        notify_parent({:saved, course_session})

        {:noreply,
         socket
         |> put_flash(:info, "Course session created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end

defmodule StudyGardenWeb.CourseSessionLive.Index do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Courses
  alias StudyGarden.Courses.CourseSession

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :course_sessions, Courses.list_course_sessions())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Course session")
    |> assign(:course_session, Courses.get_course_session!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Course session")
    |> assign(:course_session, %CourseSession{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Course sessions")
    |> assign(:course_session, nil)
  end

  @impl true
  def handle_info({StudyGardenWeb.CourseSessionLive.FormComponent, {:saved, course_session}}, socket) do
    {:noreply, stream_insert(socket, :course_sessions, course_session)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    course_session = Courses.get_course_session!(id)
    {:ok, _} = Courses.delete_course_session(course_session)

    {:noreply, stream_delete(socket, :course_sessions, course_session)}
  end
end

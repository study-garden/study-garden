defmodule StudyGardenWeb.SchoolLive.About do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Schools

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(_params, _session, socket) do
    school = Schools.get_school!()

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:school, school)}
  end

  defp page_title(:about), do: "About"

  defp description_paragraphs(description) do
    String.split(description, "\n")
  end
end

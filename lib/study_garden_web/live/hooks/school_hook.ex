defmodule StudyGardenWeb.SchoolHook do
  @moduledoc """
  LiveView hook for loading school data.
  """

  import Phoenix.Component

  alias StudyGarden.Schools

  def on_mount(:default, _params, _session, socket) do
    school = Schools.get_school!()
    school_announcement = Schools.get_latest_announcement()

    socket =
      socket
      |> assign(:school, school)
      |> assign(:school_announcement, school_announcement)

    {:cont, socket}
  end
end

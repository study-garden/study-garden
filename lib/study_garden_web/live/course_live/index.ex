defmodule StudyGardenWeb.CourseLive.Index do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Courses
  alias StudyGarden.Courses.Course

  @integer_days %{
    1 => "Monday",
    2 => "Tuesday",
    3 => "Wednesday",
    4 => "Thursday",
    5 => "Friday",
    6 => "Saturday",
    7 => "Sunday"
  }

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :courses, Courses.list_courses())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Course")
    |> assign(:course, Courses.get_course!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Course")
    |> assign(:course, %Course{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Courses")
    |> assign(:course, nil)
  end

  @impl true
  def handle_info({StudyGardenWeb.CourseLive.FormComponent, {:saved, course}}, socket) do
    {:noreply, stream_insert(socket, :courses, course)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    course = Courses.get_course!(id)
    {:ok, _} = Courses.delete_course(course)

    {:noreply, stream_delete(socket, :courses, course)}
  end

  defp human_readable_date(date) do
    Calendar.strftime(date, "%B %d")
  end

  defp day_for_session(session) do
    day = Date.day_of_week(session.start)
    Access.get(@integer_days, day)
  end

  defp days_for_sessions(sessions) do
    sessions
    |> Enum.map(fn session -> day_for_session(session) end)
    |> Enum.uniq()
  end

  defp time_with_school_timezone(date) do
    date
    |> Timex.to_datetime("America/Phoenix")
    |> Timex.format!("{h12} {am} ({Zname})")
  end
end

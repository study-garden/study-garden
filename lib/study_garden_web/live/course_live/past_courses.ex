defmodule StudyGardenWeb.CourseLive.PastCourses do
  use StudyGardenWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <.header>Past Courses</.header>

    <p class="tw-mt-3 tw-text-zinc-700">
      All recordings of past courses are available in the archive with an Auditor subscription tier or higher.
    </p>

    <p class="tw-mt-3 tw-text-zinc-700">
      You can subscribe
      <.link class="tw-text-green-700 hover:tw-underline" navigate={~p"/subscribe"}>here.</.link>
    </p>

    <div id="past-courses" class="tw-flex tw-flex-col tw-gap-1 tw-mt-6">
      <span class="tw-text-lg tw-text-green-700 tw-font-semibold">Spring 2024</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>"History of Damnation" - Sean Capener</li>
        <li>"Ecology and Empire" - Jules Delisle</li>
        <li>"Sovereignty" - Colin Drumm</li>
      </ul>

      <span class="tw-text-lg tw-text-green-700 tw-font-semibold">Fall 2023</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Modern Revolutions” (Jared Baxter)</li>
        <li>“Human Origins” (Jules Delisle)</li>
        <li>“Punic Wars” (Colin Drumm)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Summer 2023</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“What is Blackness?” (Taija McDougall)</li>
        <li>“Leviathan” (Colin Drumm)</li>
        <li>“On Horses” (Joy Shokeir)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Spring 2023</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Anti-Anthropology” (Alirio Karina)</li>
        <li>“Cybernetics and Complexity” (Mimbres Faculty)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Winter 2023</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“King Lear” (Mimbres Faculty)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Fall 2022</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Political Theology” (Colin Drumm)</li>
        <li>“Reproduction and Control” (Jules Delisle)</li>
        <li>“The Early Marx” (Jared Baxter)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Summer 2022</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Anti-Oedipus” (Colin Drumm)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Spring 2022</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Prelude to Roman History” (Colin Drumm)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Fall 2021</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Alliance, Affinity, and Interdependence” (Jules Delisle)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Summer 2021</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“What is Money?" (Colin Drumm)</li>
      </ul>
      <span class="tw-mt-4 tw-text-lg tw-text-green-700 tw-font-semibold">Spring 2021</span>
      <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
      <ul>
        <li>“Race and Finance from the Money View” (Colin Drumm)</li>
      </ul>
    </div>
    """
  end
end

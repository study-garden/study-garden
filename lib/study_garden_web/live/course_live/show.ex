defmodule StudyGardenWeb.CourseLive.Show do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Courses

  # Human-readable date format.
  @hr_date_format "%a. %B %d, %Y"

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"slug" => slug}, _, socket) do
    course = Courses.get_course_by_slug!(slug)

    hr_start_date = Calendar.strftime(course.start_date, @hr_date_format)
    hr_end_date = Calendar.strftime(course.end_date, @hr_date_format)

    {:noreply,
     socket
     |> assign(:page_title, page_title(course.title, socket.assigns.live_action))
     |> assign(:course, course)
     |> assign(:hr_start_date, hr_start_date)
     |> assign(:hr_end_date, hr_end_date)}
  end

  defp page_title(course_title, :show), do: "#{course_title}"
  defp page_title(course_title, :edit), do: "Edit #{course_title}"

  defp human_readable_date(date) do
    Calendar.strftime(date, "%B %d")
  end

  defp day_for_session(session) do
    day = Date.day_of_week(session.start)
    Access.get(@integer_days, day)
  end

  defp days_for_sessions(sessions) do
    sessions
    |> Enum.map(fn session -> day_for_session(session) end)
    |> Enum.uniq()
  end

  defp time_with_school_timezone(date) do
    date
    |> Timex.to_datetime("America/Phoenix")
    |> Timex.format!("{h12} {am} ({Zname})")
  end
end

defmodule StudyGardenWeb.AnnouncementLive.Index do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Schools
  alias StudyGarden.Schools.Announcement

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :school_announcements, Schools.list_school_announcements())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Announcement")
    |> assign(:announcement, Schools.get_announcement!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Announcement")
    |> assign(:announcement, %Announcement{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing School announcements")
    |> assign(:announcement, nil)
  end

  @impl true
  def handle_info({StudyGardenWeb.AnnouncementLive.FormComponent, {:saved, announcement}}, socket) do
    {:noreply, stream_insert(socket, :school_announcements, announcement)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    announcement = Schools.get_announcement!(id)
    {:ok, _} = Schools.delete_announcement(announcement)

    {:noreply, stream_delete(socket, :school_announcements, announcement)}
  end
end

defmodule StudyGardenWeb.UserSettingsLive do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Identities

  def render(assigns) do
    ~H"""
    <.header class="tw-text-center">
      Account Settings
      <:subtitle>Manage your account settings</:subtitle>
    </.header>

    <div class="tw-space-y-12 tw-divide-y">
      <div>
        <.simple_form
          for={@email_form}
          id="email_form"
          phx-submit="update_email"
          phx-change="validate_email"
        >
          <div>
            <div class="tw-block tw-text-sm tw-font-semibold tw-leading-6 tw-text-zinc-800">
              Current Email:
            </div>
            <div class="tw-text-sm tw-text-zinc-700"><%= @current_user.email %></div>
          </div>
          <.input field={@email_form[:email]} type="email" label="New Email" required />
          <:actions>
            <.button phx-disable-with="Changing...">Change Email</.button>
          </:actions>
        </.simple_form>
      </div>
      <div id="discord-account-info" class="tw-flex tw-flex-col">
        <span class="tw-text-green-700 tw-text-lg">Discord Account</span>
        <%= if @current_user.discord_user do %>
          <div id="discord-user-card" class="tw-bg-discord tw-mt-4 tw-p-4 tw-rounded-lg tw-text-white">
            <img
              src={~p"/images/discord-mark-white.svg"}
              class="tw-h-6 tw-w-6"
              aria-hidden="true"
              aria-labelledby="Discord Logo"
            />

            <div class="tw-flex tw-items-center tw-gap-1">
              <span class="tw-text-xl tw-font-semibold">
                <%= @current_user.discord_user.discord_username %>
              </span>
              <span class="tw-text-sm">
                (email: <%= @current_user.discord_user.discord_email %>)
              </span>
            </div>
          </div>
        <% else %>
          <.link navigate={~p"/oauth/discord"}>
            <button class={[
              "sg-leaf",
              "tw-py-2 tw-px-4 tw-text-sm tw-font-semibold tw-leading-6 tw-text-white tw-shadow",
              "active:tw-text-white/80",
              "tw-flex tw-items-center tw-gap-2 tw-bg-discord hover:tw-bg-indigo-700"
            ]}>
              <img
                src={~p"/images/discord-mark-white.svg"}
                class="tw-h-6 tw-w-6"
                aria-hidden="true"
                aria-labelledby="Discord Logo"
              />
              <span>Connect Discord Account</span>
            </button>
          </.link>
        <% end %>
      </div>
    </div>
    """
  end

  def mount(%{"token" => token}, _session, socket) do
    socket =
      case Identities.update_user_email(socket.assigns.current_user, token) do
        :ok ->
          put_flash(socket, :info, "Email changed successfully.")

        :error ->
          put_flash(socket, :error, "Email change link is invalid or it has expired.")
      end

    {:ok, push_navigate(socket, to: ~p"/users/settings")}
  end

  def mount(_params, _session, socket) do
    user = socket.assigns.current_user
    email_changeset = Identities.change_user_email(user)

    socket =
      socket
      |> assign(:current_email, user.email)
      |> assign(:email_form, to_form(email_changeset))
      |> assign(:trigger_submit, false)

    {:ok, socket}
  end

  def handle_event("validate_email", params, socket) do
    %{"user" => user_params} = params

    email_form =
      socket.assigns.current_user
      |> Identities.change_user_email(user_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, email_form: email_form)}
  end

  def handle_event("update_email", params, socket) do
    %{"user" => user_params} = params
    user = socket.assigns.current_user

    case Identities.apply_user_email(user, user_params) do
      {:ok, applied_user} ->
        Identities.deliver_user_update_email_instructions(
          applied_user,
          user.email,
          &url(~p"/users/settings/confirm_email/#{&1}")
        )

        info = "A link to confirm your email change has been sent to the new address."
        {:noreply, socket |> put_flash(:info, info)}

      {:error, changeset} ->
        {:noreply, assign(socket, :email_form, to_form(Map.put(changeset, :action, :insert)))}
    end
  end
end

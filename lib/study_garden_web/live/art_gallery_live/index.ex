defmodule StudyGardenWeb.ArtGalleryLive.Index do
  use StudyGardenWeb, :live_view

  @images [
    %{
      path: "/images/alliance-and-affinity-sm.jpg",
      name: "Alliance and Affinity"
    },
    %{
      path: "/images/anti-anthropology-sm.jpg",
      name: "Anti-Anthropology"
    },
    %{
      path: "/images/history-of-damnation-sm.jpg",
      name: "History of Damnation"
    },
    %{
      path: "/images/ecology-and-empire-sm.jpg",
      name: "Ecology and Empire"
    },
    %{
      path: "/images/human-origins-sm.jpg",
      name: "Human Origins"
    },
    %{
      path: "/images/leviathan-sm.jpg",
      name: "Leviathan"
    },
    %{
      path: "/images/modern-revolutions-sm.jpg",
      name: "Modern Revolutions"
    },
    %{
      path: "/images/on-horses-sm.jpg",
      name: "On Horses"
    },
    %{
      path: "/images/punic-wars-sm.jpg",
      name: "The Punic Wars"
    },
    %{
      path: "/images/sovereignty-sm.jpg",
      name: "Sovereignty"
    }
  ]

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:current_image_index, 0)
     |> assign(:images, @images)
     |> assign(:current_image, Enum.at(@images, 0))}
  end

  @impl true
  def handle_params(_params, _session, socket) do
    {:noreply,
     socket
     |> assign(:page_title, "Art Gallery")}
  end

  @impl true
  def handle_event("prev", _params, socket) do
    socket =
      socket
      |> update(:current_image_index, fn index ->
        if index - 1 < 0 do
          length(@images) - 1
        else
          socket.assigns.current_image_index - 1
        end
      end)
      |> update(:current_image, fn _ ->
        Enum.at(
          @images,
          socket.assigns.current_image_index - 1,
          Enum.at(@images, length(@images) - 1)
        )
      end)

    {:noreply, socket}
  end

  @impl true
  def handle_event("next", _params, socket) do
    socket =
      socket
      |> update(
        :current_image_index,
        fn index ->
          if index + 1 >= length(@images) do
            0
          else
            socket.assigns.current_image_index + 1
          end
        end
      )
      |> update(:current_image, fn _ ->
        Enum.at(@images, socket.assigns.current_image_index + 1, Enum.at(@images, 0))
      end)

    {:noreply, socket}
  end
end

defmodule StudyGardenWeb.UserRegistrationLive do
  use StudyGardenWeb, :live_view

  def render(assigns) do
    ~H"""
    <div class="tw-mx-auto tw-max-w-sm">
      <.header class="text-center">
        Register for an account
        <:subtitle>
          Already registered?
          <.link
            navigate={~p"/users/log_in"}
            class="tw-font-semibold tw-text-brand hover:tw-underline"
          >
            Sign in
          </.link>
          to your account now.
        </:subtitle>
      </.header>

      <div class="discord-oauth tw-mt-4">
        <.link navigate={~p"/oauth/discord"}>
          <button class={[
            "sg-leaf",
            "tw-py-2 tw-px-4 tw-text-sm tw-font-semibold tw-leading-6 tw-text-white tw-shadow",
            "active:tw-text-white/80",
            "tw-flex tw-items-center tw-gap-2 tw-bg-discord hover:tw-bg-indigo-700"
          ]}>
            <img
              src={~p"/images/discord-mark-white.svg"}
              class="tw-h-6 tw-w-6"
              aria-hidden="true"
              aria-labelledby="Discord Logo"
            />
            <span>Register with Discord</span>
          </button>
        </.link>
        <div class="tw-text-sm tw-text-zinc-600 tw-pt-2">
          This will register an account using your Discord email. We will send a message to that email with a one-time invite to the <%= @school.short_name %> Discord server.
        </div>
      </div>
    </div>
    """
  end

  def mount(_params, _session, socket) do
    socket =
      {:ok, socket}
  end
end

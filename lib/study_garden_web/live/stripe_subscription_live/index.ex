defmodule StudyGardenWeb.StripeSubscriptionLive.Index do
  use StudyGardenWeb, :live_view

  alias StudyGarden.Stripe
  alias StudyGarden.Stripe.StripeSubscription

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :stripe_subscriptions, Stripe.list_stripe_subscriptions())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Stripe subscription")
    |> assign(:stripe_subscription, Stripe.get_stripe_subscription!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Stripe subscription")
    |> assign(:stripe_subscription, %StripeSubscription{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Stripe subscriptions")
    |> assign(:stripe_subscription, nil)
  end

  @impl true
  def handle_info({StudyGardenWeb.StripeSubscriptionLive.FormComponent, {:saved, stripe_subscription}}, socket) do
    {:noreply, stream_insert(socket, :stripe_subscriptions, stripe_subscription)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    stripe_subscription = Stripe.get_stripe_subscription!(id)
    {:ok, _} = Stripe.delete_stripe_subscription(stripe_subscription)

    {:noreply, stream_delete(socket, :stripe_subscriptions, stripe_subscription)}
  end
end

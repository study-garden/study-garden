defmodule StudyGardenWeb.StripeSubscriptionLive.Subscribe do
  use Phoenix.Component

  use StudyGardenWeb, :live_view

  alias StudyGarden.Stripe.StripeSubscription

  attr :name, :string, required: true
  attr :price, :integer, required: true
  slot :inner_block, required: true

  def subscription_card(assigns) do
    ~H"""
    <div class="sg-leaf tw-p-4 tw-min-w-80 md:tw-min-w-96 tw-max-w-96 tw-shadow-sm tw-border tw-flex tw-flex-col tw-gap-1">
      <span class="tw-text-lg tw-text-green-700 tw-font-semibold">
        <%= @name %>
      </span>
      <span class="tw-text-zinc-700">Benefits</span>
      <ul><%= render_slot(@inner_block) %></ul>
      <span class="tw-place-self-center tw-text-green-700 tw-font-semibold">
        <%= @price %><span class="tw-text-zinc-600 tw-text-sm tw-font-normal"> / month</span>
      </span>
      <.button
        phx-click="checkout"
        phx-value-stripe_id={@stripe_id}
        class="tw-max-w-32 tw-place-self-center"
      >
        Subscribe
      </.button>
    </div>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Subscribe")
  end

  @impl true
  def handle_event("checkout", %{"stripe_id" => stripe_id}, socket) do
    # Create a Stripe checkout session for course
    endpoint_url = StudyGardenWeb.Endpoint.url()

    stripe_subscription_id = stripe_id

    stripe_price = stripe_id

    # Study Garden to Stripe key
    sg_stripe_key = Ecto.UUID.generate()

    # Create Stripe checkout session
    stripe_checkout_session_params = %{
      line_items: [
        %{
          price: stripe_id,
          quantity: 1
        }
      ],
      mode: "subscription",
      success_url: endpoint_url <> "/subscribe/checkout/success?session_id={CHECKOUT_SESSION_ID}",
      cancel_url: endpoint_url <> "/subscribe/checkout/cancelled",
      automatic_tax: %{enabled: true},
      subscription_data: %{
        metadata: %{
          "sg_product_type" => "subscription",
          "sg_stripe_key" => sg_stripe_key
        }
      }
    }

    with {:ok, stripe_checkout_session} <-
           Stripe.Checkout.Session.create(stripe_checkout_session_params) do
      {:noreply,
       socket
       |> redirect(external: stripe_checkout_session.url)}
    else
      {:error, %Stripe.ApiErrors{} = _stripe_api_error} ->
        {:noreply,
         socket
         |> put_flash(:error, "Something went wrong processing Stripe payment")
         |> push_patch(to: socket.assigns.patch)}

      {:error, reason} ->
        {:noreply,
         socket
         |> put_flash(:error, "Something went wrong processing Stripe subscription payment")}
    end
  end
end

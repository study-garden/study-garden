defmodule StudyGardenWeb.StripeSubscriptionLive.SubscribePostCheckout do
  use StudyGardenWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  defp apply_action(socket, :success, _params) do
    socket
    |> assign(:page_title, "Subscription Payment Success")
  end

  defp apply_action(socket, :cancelled, _params) do
    socket
    |> assign(:page_title, "Subscription Payment Cancelled")
  end
end

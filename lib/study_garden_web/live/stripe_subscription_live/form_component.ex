defmodule StudyGardenWeb.StripeSubscriptionLive.FormComponent do
  use StudyGardenWeb, :live_component

  alias StudyGarden.Stripe

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage stripe_subscription records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="stripe_subscription-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:stripe_id]} type="text" label="Stripe" />
        <.input
          field={@form[:stripe_cancel_at_period_end]}
          type="checkbox"
          label="Stripe cancel at period end"
        />
        <.input
          field={@form[:stripe_current_period_start]}
          type="number"
          label="Stripe current period start"
        />
        <.input
          field={@form[:stripe_current_period_end]}
          type="number"
          label="Stripe current period end"
        />
        <.input field={@form[:stripe_ended_at]} type="number" label="Stripe ended at" />
        <.input field={@form[:stripe_customer]} type="text" label="Stripe customer" />
        <.input field={@form[:stripe_latest_invoice]} type="text" label="Stripe latest invoice" />
        <.input
          field={@form[:stripe_status]}
          type="select"
          label="Stripe status"
          prompt="Choose a value"
          options={Ecto.Enum.values(StudyGarden.Stripe.StripeSubscription, :stripe_status)}
        />
        <.input field={@form[:stripe_cancel_at]} type="number" label="Stripe cancel at" />
        <.input field={@form[:stripe_canceled_at]} type="number" label="Stripe cancelled at" />
        <.input field={@form[:stripe_created]} type="number" label="Stripe created" />
        <.input field={@form[:stripe_days_until_due]} type="number" label="Stripe days until due" />
        <.input field={@form[:stripe_start_date]} type="number" label="Stripe start date" />
        <.input field={@form[:sg_stripe_key]} type="text" label="Sg stripe key" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Stripe subscription</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{stripe_subscription: stripe_subscription} = assigns, socket) do
    changeset = Stripe.change_stripe_subscription(stripe_subscription)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"stripe_subscription" => stripe_subscription_params}, socket) do
    changeset =
      socket.assigns.stripe_subscription
      |> Stripe.change_stripe_subscription(stripe_subscription_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"stripe_subscription" => stripe_subscription_params}, socket) do
    save_stripe_subscription(socket, socket.assigns.action, stripe_subscription_params)
  end

  defp save_stripe_subscription(socket, :edit, stripe_subscription_params) do
    case Stripe.update_stripe_subscription(
           socket.assigns.stripe_subscription,
           stripe_subscription_params
         ) do
      {:ok, stripe_subscription} ->
        notify_parent({:saved, stripe_subscription})

        {:noreply,
         socket
         |> put_flash(:info, "Stripe subscription updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_stripe_subscription(socket, :new, stripe_subscription_params) do
    case Stripe.create_stripe_subscription(stripe_subscription_params) do
      {:ok, stripe_subscription} ->
        notify_parent({:saved, stripe_subscription})

        {:noreply,
         socket
         |> put_flash(:info, "Stripe subscription created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end

defmodule StudyGardenWeb.Layouts do
  alias Phoenix.LiveView.JS

  use StudyGardenWeb, :html

  embed_templates "layouts/*"

  @doc """
  Renders the primary navigation bar.

  ## Examples

      <.primary_nav>Send!</.button>
      <.primary_nav phx-click="go" class="tw-ml-2">Send!</.button>
  """

  def site_header(assigns) do
    ~H"""
    <header
      id="site-header"
      class="tw-sticky tw-top-0 tw-bg-white tw-border-b tw-border-zinc-100 tw-shadow-sm tw-z-10"
    >
      <.site_nav current_user={@current_user} school={@school} />
      <.school_announcement_banner school_announcement={@school_announcement} />
    </header>
    """
  end

  def site_nav(assigns) do
    ~H"""
    <nav
      id="site-navigation"
      class="tw-px-4 tw-sm:px-6 tw-py-2 tw-flex tw-items-center tw-justify-between tw-min-h-20"
    >
      <div class="tw-flex tw-items-center tw-gap-4 tw-min-h-12">
        <button
          id="site-hamburger-menu-button"
          class="sg-leaf not-sr-only tw-p-2 tw-border tw-bg-green-700 tw-w-20 tw-h-16 tw-flex tw-items-center tw-justify-center hover:tw-bg-green-600 active:tw-bg-green-700 tw-shadow"
          phx-click={show_site_hamburger_menu()}
          phx-click-away={hide_site_hamburger_menu()}
        >
          <.icon name="tw-hero-bars-3 tw-bg-white" class="tw-h-8 tw-w-8" />
        </button>
        <.link href={~p"/"} class="tw-block tw-flex tw-items-center">
          <div
            :if={@school}
            class="tw-text-3xl tw-font-bold tw-text-green-700 hover:tw-text-green-600 hover:tw-drop-shadow-sm"
          >
            <%= @school.short_name %>
          </div>
        </.link>
      </div>
      <.site_menu current_user={@current_user} />
    </nav>
    """
  end

  def show_site_hamburger_menu(js \\ %JS{}) do
    js
    |> JS.add_class("tw-hidden", to: "#site-hamburger-menu-button")
    |> JS.remove_class("tw-hidden", to: "#site-menu-content")
  end

  def hide_site_hamburger_menu(js \\ %JS{}) do
    js
    |> JS.add_class("tw-hidden", to: "#site-menu-content")
    |> JS.remove_class("tw-hidden", to: "#site-hamburger-menu-button")
  end

  def site_menu(assigns) do
    ~H"""
    <div id="site-menu" class="tw-flex tw-place-items-center tw-gap-3">
      <%= if @current_user do %>
        <div class="user-info tw-hidden md:tw-block tw-flex tw-place-items-center tw-gap-1">
          <.icon name="tw-hero-user-circle tw-bg-green-700" class="tw-h-6 tw-w-6" />
          <span class="tw-text-zinc-600">
            <%= @current_user.email %>
          </span>
        </div>
      <% end %>
      <div
        id="site-menu-content"
        class="sg-leaf tw-hidden tw-border tw-border-2 tw-border-green-800 tw-px-10 tw-pt-8 tw-pb-6 tw-absolute tw-top-2 tw-left-2 tw-bg-white tw-flex tw-flex-col"
        phx-click-away={hide_site_hamburger_menu()}
      >
        <button id="site-menu-content-close-button" phx-click={hide_site_hamburger_menu()}>
          <.icon name="tw-hero-x-mark" class="tw-h-6 tw-w-6 tw-absolute tw-top-2 tw-right-2" />
        </button>
        <nav id="site-menu-nav" class="tw-divide-y tw-flex tw-flex-col tw-gap-3">
          <ul class="tw-flex tw-flex-col tw-gap-3">
            <div class="tw-flex tw-flex-col">
              <.link
                navigate={~p"/about"}
                class="tw-flex tw-place-items-center tw-gap-1 tw-text-zinc-800 hover:tw-underline"
              >
                <.icon name="tw-hero-information-circle tw-bg-green-700" class="tw-h-4 tw-w-4" />
                About Us
              </.link>
              <.link
                navigate={~p"/calendar"}
                class="tw-flex tw-gap-1 tw-text-zinc-800 tw-place-items-center hover:tw-underline"
              >
                <.icon name="tw-hero-calendar-days tw-bg-green-700" class="tw-h-4 tw-w-4" />
                School Calendar
              </.link>
            </div>

            <div class="courses-nav tw-flex tw-flex-col">
              <div class="tw-flex tw-place-items-center tw-gap-1">
                <.icon name="tw-hero-book-open tw-bg-green-700" class="tw-h-4 tw-w-4" />
                <span class="tw-text-green-700 tw-font-semibold">Courses</span>
              </div>
              <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
              <.link href={~p"/courses"} class="tw-text-zinc-800 hover:tw-underline">
                Summer 2024
              </.link>
              <.link href={~p"/courses/past"} class="tw-text-zinc-800 hover:tw-underline">
                Past Courses
              </.link>
            </div>

            <div class="support-us-nav tw-flex tw-flex-col">
              <div class="tw-flex tw-place-items-center tw-gap-1">
                <.icon name="tw-hero-banknotes tw-bg-green-700" class="tw-h-4 tw-w-4" />
                <span class="tw-text-green-700 tw-font-semibold">Support Us</span>
              </div>
              <div class="tw-h-px tw-my-2 tw-bg-gray-200 tw-border-0"></div>
              <.link navigate={~p"/subscribe"} class="tw-text-zinc-800 hover:tw-underline">
                Subscriptions
              </.link>
              <a
                href="https://buy.stripe.com/9AQg2n3V9f7OeiseUZ"
                class="tw-text-zinc-800 hover:tw-underline"
              >
                One-Time Donation
              </a>
            </div>

            <div class="tw-h-px tw-bg-gray-200 tw-border-0"></div>

            <.link
              navigate={~p"/art-gallery"}
              class="tw-flex tw-place-items-center tw-gap-1 hover:tw-underline"
            >
              <.icon name="tw-hero-photo tw-bg-green-700" class="tw-h-4 tw-w-4" />
              <span class="tw-text-green-700 tw-font-semibold">Art Gallery</span>
            </.link>
            <div class="tw-h-px tw-bg-gray-200 tw-border-0"></div>

            <div class="tw-flex tw-flex-col">
              <.link
                class="tw-flex tw-place-items-center tw-gap-1 hover:tw-underline tw-text-zinc-800"
                navigate={~p"/contact"}
              >
                <.icon name="tw-hero-envelope tw-bg-green-700" class="tw-h-4 tw-w-4" /> Contact
              </.link>
              <.link
                class="tw-flex tw-place-items-center tw-gap-1 hover:tw-underline tw-text-zinc-800"
                navigate={~p"/terms-of-service"}
              >
                <.icon name="tw-hero-document-text tw-bg-green-700" class="tw-h-4 tw-w-4" />
                Terms of Service
              </.link>
              <.link
                class="tw-flex tw-place-items-center tw-gap-1 hover:tw-underline tw-text-zinc-800"
                navigate={~p"/help"}
              >
                <.icon name="tw-hero-question-mark-circle tw-bg-green-700" class="tw-h-4 tw-w-4" />
                Help
              </.link>
            </div>
          </ul>

          <ul class="tw-flex tw-flex-col tw-gap-1 tw-pt-4">
            <li>
              <a
                href="https://billing.stripe.com/p/login/4gwcOCgbi3zQaPK4gg"
                class="tw-text-green-800 hover:tw-underline"
              >
                Manage My Subscription
              </a>
            </li>
            <%= if @current_user do %>
              <li>
                <.link href={~p"/users/settings"} class="tw-text-green-800 hover:tw-underline">
                  Settings
                </.link>
              </li>
              <li>
                <.link
                  href={~p"/users/log_out"}
                  method="delete"
                  class="tw-text-green-800 hover:tw-underline"
                >
                  Log out
                </.link>
              </li>
            <% else %>
              <li>
                <.link href={~p"/users/register"} class="tw-text-green-800 hover:tw-underline">
                  Register
                </.link>
              </li>
              <li>
                <.link href={~p"/users/log_in"} class="tw-text-green-800 hover:tw-underline">
                  Log in
                </.link>
              </li>
            <% end %>
          </ul>
        </nav>
      </div>
    </div>
    """
  end

  def school_announcement_banner(assigns) do
    ~H"""
    <div
      :if={@school_announcement}
      class="school-announcement-banner tw-p-2 tw-bg-green-700 tw-text-zinc-50 tw-text-sm tw-flex tw-place-content-center"
    >
      <span><%= raw(@school_announcement.message) %></span>
    </div>
    """
  end
end

defmodule StudyGardenWeb.Plugs.School do
  @moduledoc """
  Plug for loading school data.
  """

  import Plug.Conn

  alias StudyGarden.Schools

  def init(opts), do: opts

  def call(%Plug.Conn{} = conn, _opts) do
    school = Schools.get_school!()
    school_announcement = Schools.get_latest_announcement()

    conn
    |> assign(:school, school)
    |> assign(:school_announcement, school_announcement)
  end
end

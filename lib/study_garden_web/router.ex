defmodule StudyGardenWeb.Router do
  use StudyGardenWeb, :router

  import StudyGardenWeb.UserAuth

  import PhoenixStorybook.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {StudyGardenWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
    plug StudyGardenWeb.Plugs.School
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", StudyGardenWeb do
    pipe_through :browser

    get "/help", PageController, :help
    get "/calendar", PageController, :calendar
    get "/contact", PageController, :contact
    get "/terms-of-service", PageController, :terms_of_service
  end

  # Other scopes may use custom stacks.
  # scope "/api", StudyGardenWeb do
  #   pipe_through :api
  # end

  # Webhook routes
  scope "/webhooks", StudyGardenWeb do
    post "/stripe", StripeController, :create
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:study_garden, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/" do
      storybook_assets()
    end

    live_storybook("/dev/storybook", backend_module: StudyGardenWeb.Storybook)

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard",
        metrics: StudyGardenWeb.Telemetry,
        ecto_repos: [StudyGarden.Repo]

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## School routes
  scope "/", StudyGardenWeb do
    pipe_through [:browser]

    live_session :school,
      on_mount: [{StudyGardenWeb.UserAuth, :mount_current_user}, StudyGardenWeb.SchoolHook] do
      live "/", SchoolLive.About, :about
      live "/about", SchoolLive.About, :about
      live "/announcements", AnnouncementLive.Index, :index
      # live "/announcements/new", AnnouncementLive.Index, :new
      # live "/announcements/:id/edit", AnnouncementLive.Index, :edit

      live "/announcements/:id", AnnouncementLive.Show, :show
      # live "/announcements/:id/show/edit", AnnouncementLive.Show, :edit
    end
  end

  ## Art Gallery
  scope "/", StudyGardenWeb do
    pipe_through [:browser]

    live_session :art_gallery,
      on_mount: [{StudyGardenWeb.UserAuth, :mount_current_user}, StudyGardenWeb.SchoolHook] do
      live "/art-gallery", ArtGalleryLive.Index, :index
    end
  end

  ## Course routes
  scope "/", StudyGardenWeb do
    pipe_through [:browser]

    live_session :courses,
      on_mount: [{StudyGardenWeb.UserAuth, :mount_current_user}, StudyGardenWeb.SchoolHook] do
      live "/courses", CourseLive.Index, :index
      live "/courses/past", CourseLive.PastCourses, :index

      live "/courses/:slug", CourseLive.Show, :show
      live "/courses/:course_slug/sign-up", CourseSignUpLive.SignUp, :new
      live "/courses/:course_slug/sign-up", CourseSignUpLive.SignUp, :checkout
      live "/courses/:course_slug/sign-up/success", CourseSignUpLive.SignUpPostCheckout, :success

      live "/courses/:course_slug/sign-up/cancelled",
           CourseSignUpLive.SignUpPostCheckout,
           :cancelled

      # live "/courses/:course_id/sign-up/:id/edit", CourseSignUpLive.SignUp, :edit

      # live "/courses/sign-ups/:id", CourseSignUpLive.Show, :show
      # live "/courses/sign_ups/:id/show/edit", CourseSignUpLive.Show, :edit
    end
  end

  ## Admin routes
  scope "/admin", StudyGardenWeb do
    pipe_through [:browser]

    live_session :admin,
      on_mount: [
        {StudyGardenWeb.UserAuth, :mount_current_user},
        {StudyGardenWeb.UserAuth, :ensure_authenticated},
        {StudyGardenWeb.UserAuth, :require_admin},
        StudyGardenWeb.SchoolHook
      ] do
      live "/courses/sign-ups", CourseSignUpLive.Index, :index
    end
  end

  ## Subscription routes
  scope "/", StudyGardenWeb do
    pipe_through [:browser]

    live_session :subscribe,
      on_mount: [{StudyGardenWeb.UserAuth, :mount_current_user}, StudyGardenWeb.SchoolHook] do
      live "/subscribe", StripeSubscriptionLive.Subscribe, :index
      live "/subscribe/:stripe_id/checkout", StripeSubscriptionLive.Subscribe, :checkout
      live "/subscribe/checkout/success", StripeSubscriptionLive.SubscribePostCheckout, :success

      live "/subscribe/checkout/cancelled",
           StripeSubscriptionLive.SubscribePostCheckout,
           :cancelled
    end
  end

  ## Authentication routes
  scope "/", StudyGardenWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [
        {StudyGardenWeb.UserAuth, :redirect_if_user_is_authenticated},
        StudyGardenWeb.SchoolHook
      ] do
      live "/users/register", UserRegistrationLive, :new
      live "/users/log_in", UserLoginLive, :new
      get "/users/log_in/email/token/:token", UserSessionController, :login_with_token
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/", StudyGardenWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{StudyGardenWeb.UserAuth, :ensure_authenticated}, StudyGardenWeb.SchoolHook] do
      live "/users/settings", UserSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email
    end
  end

  ## OAuth routes
  scope "/oauth", StudyGardenWeb do
    pipe_through :browser

    get "/:provider", OAuthController, :request
    get "/:provider/callback", OAuthController, :callback
  end

  scope "/", StudyGardenWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete

    live_session :current_user,
      on_mount: [{StudyGardenWeb.UserAuth, :mount_current_user}, StudyGardenWeb.SchoolHook] do
      live "/users/confirm/:token", UserConfirmationLive, :edit
      live "/users/confirm", UserConfirmationInstructionsLive, :new
    end
  end
end

defmodule StudyGardenWeb.UserSessionController do
  use StudyGardenWeb, :controller

  alias StudyGarden.Identities
  alias StudyGarden.Identities.User
  alias StudyGardenWeb.UserAuth

  def send_magic_link(conn, params) do
    %{"user" => %{"email" => email}} = params

    Identities.login_or_register_user(email)

    conn
    |> put_flash(:info, "We've sent an email to #{email}, with a one-time sign-in link.")
    |> redirect(to: ~p"/users/log_in")
  end

  def login_with_token(conn, %{"token" => token} = _params) do
    case Identities.get_user_by_email_token(token, "email_login_link") do
      %User{} = user ->
        {:ok, user} = Identities.confirm_user(user)

        conn
        |> put_flash(:info, "Logged in successfully.")
        |> UserAuth.log_in_user(user)

      _ ->
        conn
        |> put_flash(:error, "That link didn't seem to work. Please try again.")
        |> redirect(to: ~p"/users/log_in")
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserAuth.log_out_user()
  end
end

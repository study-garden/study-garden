defmodule StudyGardenWeb.PageHTML do
  use StudyGardenWeb, :html

  embed_templates "page_html/*"
end

defmodule StudyGardenWeb.OAuthController do
  use StudyGardenWeb, :controller

  alias StudyGarden.Identities
  alias StudyGardenWeb.UserAuth

  plug Ueberauth

  def callback(%{assigns: %{ueberauth_auth: auth_data}} = conn, _params) do
    if auth_data.provider == :discord do
      case Identities.register_user_from_discord(auth_data) do
        {:ok, user} ->
          conn
          |> put_flash(:info, "Account created through Discord successfully.")
          |> UserAuth.log_in_user(user)

        {:error, _reason} ->
          conn
          |> put_flash(:error, "Something went wrong registering through Discord!")
          |> redirect(to: ~p"/users/register")
      end
    end

    conn
  end

  def callback(%{assigns: %{ueberauth_failure: _}} = conn, _params) do
    conn
    |> put_flash(:error, "Authentication failed.")
    |> redirect(to: ~p"/")
  end
end

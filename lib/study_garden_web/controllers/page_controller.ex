defmodule StudyGardenWeb.PageController do
  use StudyGardenWeb, :controller

  def help(conn, _params) do
    render(conn, :help)
  end

  def home(conn, _params) do
    render(conn, :home)
  end

  def calendar(conn, _params) do
    render(conn, :calendar)
  end

  def contact(conn, _params) do
    render(conn, :contact)
  end

  def terms_of_service(conn, _params) do
    render(conn, :terms_of_service)
  end
end

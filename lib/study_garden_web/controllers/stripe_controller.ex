defmodule StudyGardenWeb.StripeController do
  @moduledoc """
  Controller for Stripe webhook events.
  """
  use StudyGardenWeb, :controller

  import Stripe.Webhook

  alias StudyGarden.Stripe, as: SGStripe

  def create(conn, _params) do
    payload =
      StudyGardenWeb.Plugs.CachedBodyReader.get_raw_body(conn)

    [signature] = get_req_header(conn, "stripe-signature")
    secret = System.get_env("STRIPE_WEBHOOK_SECRET")

    case construct_event(payload, signature, secret) do
      {:ok, %Stripe.Event{} = event} ->
        SGStripe.get_or_create_stripe_webhook_event(%{
          host: conn.host,
          stripe_id: event.id,
          stripe_request_id: event.request.id,
          stripe_api_version: event.api_version,
          stripe_created: event.created,
          stripe_idempotency_key: event.request.idempotency_key,
          stripe_type: event.type,
          stripe_signature: signature
        })

        send_resp(conn, 200, "")

      {:error, reason} ->
        send_resp(conn, 500, reason)
    end
  end
end

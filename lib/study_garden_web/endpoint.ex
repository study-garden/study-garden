defmodule StudyGardenWeb.Endpoint do
  # # Only run Sentry plugs in production
  # if Mix.env() == :prod do
  #   use Sentry.PlugCapture
  # end

  use Phoenix.Endpoint, otp_app: :study_garden

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  @session_options [
    store: :cookie,
    key: "_study_garden_key",
    signing_salt: "+EQ3fluo",
    same_site: "Lax"
  ]

  socket "/live", Phoenix.LiveView.Socket,
    websocket: [connect_info: [session: @session_options]],
    longpoll: [connect_info: [session: @session_options]]

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :study_garden,
    gzip: false,
    only: StudyGardenWeb.static_paths()

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
    plug Phoenix.Ecto.CheckRepoStatus, otp_app: :study_garden
  end

  plug Phoenix.LiveDashboard.RequestLogger,
    param_key: "request_logger",
    cookie_key: "request_logger"

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  # `CachedBodyReader` is needed to process Stripe webhook events.
  plug Plug.Parsers,
    parsers: [:urlencoded, :json],
    pass: ["*/*"],
    body_reader: {StudyGardenWeb.Plugs.CachedBodyReader, :read_body, []},
    json_decoder: Phoenix.json_library()

  # # Only run Sentry plugs in production
  # if Mix.env() == :prod do
  #   plug Sentry.PlugContext
  # end

  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options
  plug StudyGardenWeb.Router
end

defmodule StudyGardenWeb.Storybook do
  @moduledoc """
  Module for Storybook configurations.
  """

  use PhoenixStorybook,
    otp_app: :study_garden_web,
    content_path: Path.expand("../../storybook", __DIR__),
    # assets path are remote path, not local file-system paths
    css_path: "/assets/storybook.css",
    js_path: "/assets/storybook.js",
    sandbox_class: "sg"
end

defmodule StudyGarden.Identities.DiscordUser do
  @moduledoc """
  Discord user schema, including OAuth credential token information.
  """
  import Ecto.Changeset

  use StudyGarden.Schema

  alias StudyGarden.Identities.User

  schema "discord_users" do
    field :discord_id, :string
    field :discord_username, :string
    field :discord_email, :string
    field :discord_verified, :boolean, default: false
    field :access_token, StudyGarden.Ecto.Encrypted.Binary
    field :access_token_expires_at, :integer
    field :refresh_token, :string

    belongs_to :user, StudyGarden.Identities.User

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(discord_user, attrs) do
    discord_user
    |> cast(attrs, [
      :discord_id,
      :discord_username,
      :discord_email,
      :discord_verified,
      :access_token,
      :access_token_expires_at,
      :refresh_token
    ])
    |> cast_assoc(:user, with: &User.oauth_registration_changeset/2)
    |> validate_required([
      :discord_id,
      :discord_username,
      :discord_email,
      :discord_verified
    ])
    |> unique_constraint(:discord_id)
  end
end

defmodule StudyGarden.Identities.UserNotifier do
  @moduledoc """
  User notifier for emails.
  """

  import Swoosh.Email
  require Nostrum.Api

  alias StudyGarden.Mailer
  alias StudyGarden.Schools

  # Delivers the email using the application mailer.
  defp deliver(recipient, subject_content, body) do
    school = Schools.get_school!()
    subject = school.short_name <> "Study Garden - " <> subject_content

    email =
      new()
      |> to(recipient)
      |> from({school.full_name, "noreply@mimbres.study.garden"})
      |> subject(subject)
      |> text_body(body)

    with {:ok, _metadata} <- Mailer.deliver(email) do
      {:ok, email}
    end
  end

  @doc """
  Deliver instructions to confirm account.
  """
  def deliver_confirmation_instructions(user, url) do
    deliver(user.email, "Confirm your account", """

    ==============================

    Hi #{user.email},

    You can confirm your account by visiting the URL below:

    #{url}

    If you didn't create an account with us, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to reset a user password.
  """
  def deliver_reset_password_instructions(user, url) do
    deliver(user.email, "Reset password instructions", """

    ==============================

    Hi #{user.email},

    You can reset your password by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to update a user email.
  """
  def deliver_update_email_instructions(user, url) do
    deliver(user.email, "Update email instructions", """

    ==============================

    Hi #{user.email},

    You can change your email by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver Discord OAuth user registration success message.
  """
  def deliver_discord_oauth_success(user, discord_user) do
    school = Schools.get_school!()

    discord_guild_invite =
      Nostrum.Api.create_channel_invite!(
        String.to_integer(System.get_env("DISCORD_GUILD_INVITE_CHANNEL_ID")),
        max_uses: 1,
        temporary: true,
        unique: true
      )

    deliver(user.email, "Successfully registered with Discord account", """

    ==============================

    Hi #{discord_user.discord_username},

    You've successfully registered for a #{school.short_name} Study Garden account through Discord. The primary email for the Study Garden account is #{user.email}.

    If you're not already in the #{school.short_name} Discord server, use this one-time invite link to join:

    https://discord.gg/#{discord_guild_invite.code}

    Our subscription system is currently experiencing some difficulties. Please join Discord and post in the #front-office and you will be given acceess. Thank you for your patience.
    =======
    """)
  end

  @doc """
  Deliver registration link.
  """
  def deliver_registration_link(user, url) do
    deliver(user.email, "Register your Mimbres Study Garden account", """

    ==============================

    Hi #{user.email},

    Please use the following link to register your account:

    #{url}

    If you didn't request this email, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver login link.
  """
  def deliver_login_link(user, url) do
    deliver(user.email, "Sign in to the Mimbres Study Garden", """

    ==============================

    Hi #{user.email},

    Please use the following link to log into your account:

    #{url}

    If you didn't request this email, please ignore this.

    ==============================
    """)
  end
end

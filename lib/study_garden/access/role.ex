defmodule StudyGarden.Access.Role do
  @moduledoc """
  Role schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "roles" do
    field :name, :string

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end

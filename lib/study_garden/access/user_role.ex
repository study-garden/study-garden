defmodule StudyGarden.Access.UserRole do
  @moduledoc """
  User role schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "user_roles" do
    belongs_to :user, StudyGarden.Identities.User
    belongs_to :role, StudyGarden.Access.Role

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(user_role, attrs) do
    user_role
    |> cast(attrs, [:user_id, :role_id])
    |> validate_required([:user_id, :role_id])
  end
end

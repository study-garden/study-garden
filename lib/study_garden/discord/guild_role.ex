defmodule StudyGarden.Discord.GuildRole do
  @moduledoc """
  The Discord guild role schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "discord_guild_roles" do
    field :discord_id, :string
    field :discord_name, :string
    field :discord_position, :integer
    field :discord_color, :integer
    field :discord_managed, :boolean, default: false

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(guild_role, attrs) do
    guild_role
    |> cast(attrs, [
      :discord_id,
      :discord_name,
      :discord_position,
      :discord_color,
      :discord_managed
    ])
    |> validate_required([
      :discord_id,
      :discord_name,
      :discord_position,
      :discord_color,
      :discord_managed
    ])
    |> unique_constraint(:discord_id)
  end
end

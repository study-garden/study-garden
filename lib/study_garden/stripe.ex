defmodule StudyGarden.Stripe do
  @moduledoc """
  The Stripe context.
  """
  require Logger

  import Ecto.Query, warn: false
  alias StudyGarden.Repo

  alias StudyGarden.Stripe.{StripeProduct, StripeWebhookEvent, StripeCustomer}

  @stripe_api_list_limit 100

  @doc """
  Returns the list of stripe_webhook_events.

  ## Examples

      iex> list_stripe_webhook_events()
      [%StripeWebhookEvent{}, ...]

  """
  def list_stripe_webhook_events do
    Repo.all(StripeWebhookEvent)
  end

  @doc """
  Gets a single stripe_webhook_event.

  Raises `Ecto.NoResultsError` if the Stripe webhook event does not exist.

  ## Examples

      iex> get_stripe_webhook_event!(123)
      %StripeWebhookEvent{}

      iex> get_stripe_webhook_event!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stripe_webhook_event!(id), do: Repo.get!(StripeWebhookEvent, id)

  @doc """
  Creates a stripe_webhook_event.

  ## Examples

      iex> create_stripe_webhook_event(%{field: value})
      {:ok, %StripeWebhookEvent{}}

      iex> create_stripe_webhook_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stripe_webhook_event(attrs \\ %{}) do
    %StripeWebhookEvent{}
    |> StripeWebhookEvent.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Gets or creates a stripe_webhook_event.

  ## Examples

      iex> get_or_create_stripe_webhook_event(%{field: value})
      {:ok, %StripeWebhookEvent{}}

      iex> get_or_create_stripe_webhook_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def get_or_create_stripe_webhook_event(attrs \\ %{}) do
    %StripeWebhookEvent{}
    |> StripeWebhookEvent.changeset(attrs)
    |> Repo.insert(
      on_conflict: {:replace_all_except, [:id, :stripe_id, :stripe_idempotency_key]},
      conflict_target: :stripe_id
    )
  end

  @doc """
  Updates a stripe_webhook_event.

  ## Examples

      iex> update_stripe_webhook_event(stripe_webhook_event, %{field: new_value})
      {:ok, %StripeWebhookEvent{}}

      iex> update_stripe_webhook_event(stripe_webhook_event, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stripe_webhook_event(%StripeWebhookEvent{} = stripe_webhook_event, attrs) do
    stripe_webhook_event
    |> StripeWebhookEvent.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a stripe_webhook_event.

  ## Examples

      iex> delete_stripe_webhook_event(stripe_webhook_event)
      {:ok, %StripeWebhookEvent{}}

      iex> delete_stripe_webhook_event(stripe_webhook_event)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stripe_webhook_event(%StripeWebhookEvent{} = stripe_webhook_event) do
    Repo.delete(stripe_webhook_event)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stripe_webhook_event changes.

  ## Examples

      iex> change_stripe_webhook_event(stripe_webhook_event)
      %Ecto.Changeset{data: %StripeWebhookEvent{}}

  """
  def change_stripe_webhook_event(%StripeWebhookEvent{} = stripe_webhook_event, attrs \\ %{}) do
    StripeWebhookEvent.changeset(stripe_webhook_event, attrs)
  end

  @doc """
  Returns the list of stripe_products.

  ## Examples

      iex> list_stripe_products()
      [%StripeProduct{}, ...]

  """
  def list_stripe_products do
    Repo.all(StripeProduct)
  end

  @doc """
  Gets a single stripe_product.

  Raises `Ecto.NoResultsError` if the Stripe product does not exist.

  ## Examples

      iex> get_stripe_product!(123)
      %StripeProduct{}

      iex> get_stripe_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stripe_product!(id), do: Repo.get!(StripeProduct, id)

  @doc """
  Creates a stripe_product.

  ## Examples

      iex> create_stripe_product(%{field: value})
      {:ok, %StripeProduct{}}

      iex> create_stripe_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stripe_product(attrs \\ %{}) do
    %StripeProduct{}
    |> StripeProduct.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stripe_product.

  ## Examples

      iex> update_stripe_product(stripe_product, %{field: new_value})
      {:ok, %StripeProduct{}}

      iex> update_stripe_product(stripe_product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stripe_product(%StripeProduct{} = stripe_product, attrs) do
    stripe_product
    |> StripeProduct.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a stripe_product.

  ## Examples

      iex> delete_stripe_product(stripe_product)
      {:ok, %StripeProduct{}}

      iex> delete_stripe_product(stripe_product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stripe_product(%StripeProduct{} = stripe_product) do
    Repo.delete(stripe_product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stripe_product changes.

  ## Examples

      iex> change_stripe_product(stripe_product)
      %Ecto.Changeset{data: %StripeProduct{}}

  """
  def change_stripe_product(%StripeProduct{} = stripe_product, attrs \\ %{}) do
    StripeProduct.changeset(stripe_product, attrs)
  end

  @doc """
  Syncs Stripe product through the Stripe API to the database.

  ## Examples

      iex> sync_stripe_product()
      {:ok, %StripeProduct{}}

  """
  def sync_stripe_product(%Stripe.Product{} = product) do
    Logger.info("Syncing Stripe product to database")

    params = %{
      stripe_id: product.id,
      stripe_name: product.name,
      stripe_active: product.active,
      default_price: product.default_price,
      stripe_metadata: product.metadata,
      stripe_created: product.created,
      stripe_updated: product.updated,
      stripe_url: product.url
    }

    %StripeProduct{}
    |> StripeProduct.changeset(params)
    |> Repo.insert(
      on_conflict: {:replace_all_except, [:id, :created_at]},
      conflict_target: :stripe_id,
      returning: true
    )
  end

  @doc """
  Syncs Stripe products through the Stripe API to the database.

  ## Examples

      iex> sync_stripe_products()
      {:ok, [%StripeProduct{}, ...]}

  """
  def sync_stripe_products() do
    Logger.info("Syncing Stripe products to database")

    case Stripe.Product.list(limit: @stripe_api_list_limit) do
      {:ok, stripe_list} ->
        products = stripe_list.data
        Enum.each(products, fn product -> sync_stripe_product(product) end)

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Returns the list of stripe_customers.

  ## Examples

      iex> list_stripe_customers()
      [%StripeCustomer{}, ...]

  """
  def list_stripe_customers do
    Repo.all(StripeCustomer)
  end

  @doc """
  Gets a single stripe_customer.

  Raises `Ecto.NoResultsError` if the Stripe customer does not exist.

  ## Examples

      iex> get_stripe_customer!(123)
      %StripeCustomer{}

      iex> get_stripe_customer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stripe_customer!(id), do: Repo.get!(StripeCustomer, id)

  @doc """
  Creates a stripe_customer.

  ## Examples

      iex> create_stripe_customer(%{field: value})
      {:ok, %StripeCustomer{}}

      iex> create_stripe_customer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stripe_customer(attrs \\ %{}) do
    %StripeCustomer{}
    |> StripeCustomer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stripe_customer.

  ## Examples

      iex> update_stripe_customer(stripe_customer, %{field: new_value})
      {:ok, %StripeCustomer{}}

      iex> update_stripe_customer(stripe_customer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stripe_customer(%StripeCustomer{} = stripe_customer, attrs) do
    stripe_customer
    |> StripeCustomer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a stripe_customer.

  ## Examples

      iex> delete_stripe_customer(stripe_customer)
      {:ok, %StripeCustomer{}}

      iex> delete_stripe_customer(stripe_customer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stripe_customer(%StripeCustomer{} = stripe_customer) do
    Repo.delete(stripe_customer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stripe_customer changes.

  ## Examples

      iex> change_stripe_customer(stripe_customer)
      %Ecto.Changeset{data: %StripeCustomer{}}

  """
  def change_stripe_customer(%StripeCustomer{} = stripe_customer, attrs \\ %{}) do
    StripeCustomer.changeset(stripe_customer, attrs)
  end

  @doc """
  Syncs Stripe customer through the Stripe API to the database.

  ## Examples

      iex> sync_stripe_customer()
      {:ok, %StripeCustomer{}}

  """
  def sync_stripe_customer(%Stripe.Customer{} = customer) do
    Logger.debug("Syncing Stripe customer to database")

    %StripeCustomer{}
    |> StripeCustomer.changeset(%{stripe_id: customer.id})
    |> Repo.insert(on_conflict: :nothing)
  end

  @doc """
  Syncs Stripe customers through the Stripe API to the database.

  ## Examples

      iex> sync_stripe_customers()
      {:ok, [%StripeCustomer{}, ...]}

  """
  def sync_stripe_customers() do
    Logger.info("Syncing Stripe customers to database")

    case Stripe.Customer.list(limit: @stripe_api_list_limit) do
      {:ok, stripe_list} ->
        customers = stripe_list.data
        Enum.each(customers, fn customer -> sync_stripe_customer(customer) end)

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Retrieves or creates a customer on Stripe.

  ## Examples

      iex> find_or_create_customer(id)
      {:ok, [%StripeCustomer{}, ...]}

  """
  def find_or_create_customer(id, customer_params) do
    case Stripe.Customer.retrieve(id) do
      {:ok, customer} ->
        {:ok, customer}

      {:error, %Stripe.Error{extra: %{http_status: 404}} = stripe_api_error} ->
        customer = Stripe.Customer.create(customer_params)

        {:ok, customer}

      {:error, reason} ->
        {:error, reason}
    end
  end

  alias StudyGarden.Stripe.StripeSubscription

  @doc """
  Returns the list of stripe_subscriptions.

  ## Examples

      iex> list_stripe_subscriptions()
      [%StripeSubscription{}, ...]

  """
  def list_stripe_subscriptions do
    Repo.all(StripeSubscription)
  end

  @doc """
  Gets a single stripe_subscription.

  Raises `Ecto.NoResultsError` if the Stripe subscription does not exist.

  ## Examples

      iex> get_stripe_subscription!(123)
      %StripeSubscription{}

      iex> get_stripe_subscription!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stripe_subscription!(id), do: Repo.get!(StripeSubscription, id)

  @doc """
  Creates a stripe_subscription.

  ## Examples

      iex> create_stripe_subscription(%{field: value})
      {:ok, %StripeSubscription{}}

      iex> create_stripe_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stripe_subscription(attrs \\ %{}) do
    %StripeSubscription{}
    |> StripeSubscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stripe_subscription.

  ## Examples

      iex> update_stripe_subscription(stripe_subscription, %{field: new_value})
      {:ok, %StripeSubscription{}}

      iex> update_stripe_subscription(stripe_subscription, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stripe_subscription(%StripeSubscription{} = stripe_subscription, attrs) do
    stripe_subscription
    |> StripeSubscription.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a stripe_subscription.

  ## Examples

      iex> delete_stripe_subscription(stripe_subscription)
      {:ok, %StripeSubscription{}}

      iex> delete_stripe_subscription(stripe_subscription)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stripe_subscription(%StripeSubscription{} = stripe_subscription) do
    Repo.delete(stripe_subscription)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stripe_subscription changes.

  ## Examples

      iex> change_stripe_subscription(stripe_subscription)
      %Ecto.Changeset{data: %StripeSubscription{}}

  """
  def change_stripe_subscription(%StripeSubscription{} = stripe_subscription, attrs \\ %{}) do
    StripeSubscription.changeset(stripe_subscription, attrs)
  end

  @doc """
  Syncs Stripe subscription through the Stripe API to the database.

  ## Examples

      iex> sync_stripe_subscription()
      {:ok, %StripeSubscription{}}

  """
  def sync_stripe_subscription(%Stripe.Subscription{} = subscription) do
    Logger.debug("Syncing Stripe subscription to database")

    %StripeSubscription{}
    |> StripeSubscription.changeset(%{
      stripe_id: subscription.id,
      stripe_cancel_at_period_end: subscription.cancel_at_period_end,
      stripe_current_period_start: subscription.current_period_start,
      stripe_current_period_end: subscription.current_period_end,
      stripe_ended_at: subscription.ended_at,
      stripe_customer: subscription.customer,
      # stripe_items: :map
      stripe_latest_invoice: subscription.latest_invoice,
      stripe_metadata: subscription.metadata,
      stripe_status: subscription.status,
      stripe_cancel_at: subscription.cancel_at,
      stripe_canceled_at: subscription.canceled_at,
      stripe_created: subscription.created,
      stripe_days_until_due: subscription.days_until_due,
      stripe_start_date: subscription.start_date,
      sg_stripe_key: subscription.metadata["sg_stripe_key"]
    })
    |> Repo.insert(
      on_conflict: {:replace_all_except, [:id, :created_at]},
      conflict_target: :stripe_id,
      returning: true
    )
  end

  @doc """
  Syncs Stripe subscriptions through the Stripe API to the database.

  ## Examples

      iex> sync_stripe_subscriptions()
      {:ok, [%StripeSubscription{}, ...]}

  """
  def sync_stripe_subscriptions() do
    Logger.info("Syncing Stripe subscriptions to database")

    case Stripe.Subscription.list(limit: @stripe_api_list_limit) do
      {:ok, stripe_list} ->
        subscriptions = stripe_list.data

        Enum.each(subscriptions, fn subscription ->
          sync_stripe_subscription(subscription)
        end)

      {:error, reason} ->
        {:error, reason}
    end
  end
end

defmodule StudyGarden.Courses do
  @moduledoc """
  The Courses context.
  """

  import Ecto.Query, warn: false
  alias StudyGarden.Repo

  alias StudyGarden.Courses.{Course, CourseSession, CourseSignUpNotifier}

  @doc """
  Returns the list of courses.

  ## Examples

      iex> list_courses()
      [%Course{}, ...]

  """
  def list_courses do
    session_query =
      from s in CourseSession, order_by: s.start

    Repo.all(Course) |> Repo.preload(sessions: session_query)
  end

  @doc """
  Gets a single course.

  Raises `Ecto.NoResultsError` if the Course does not exist.

  ## Examples

      iex> get_course!(123)
      %Course{}

      iex> get_course!(456)
      ** (Ecto.NoResultsError)

  """
  def get_course!(id), do: Repo.get!(Course, id) |> Repo.preload([:sessions])

  @doc """
  Gets a single course.

  Raises `Ecto.NoResultsError` if the Course does not exist.

  ## Examples

      iex> get_course_by_slug!(123)
      %Course{}

      iex> get_course_by_slug!(456)
      ** (Ecto.NoResultsError)

  """
  def get_course_by_slug!(slug), do: Repo.get_by!(Course, slug: slug) |> Repo.preload([:sessions])

  @doc """
  Creates a course.

  ## Examples

      iex> create_course(%{field: value})
      {:ok, %Course{}}

      iex> create_course(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_course(attrs \\ %{}) do
    %Course{}
    |> Course.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a course.

  ## Examples

      iex> update_course(course, %{field: new_value})
      {:ok, %Course{}}

      iex> update_course(course, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_course(%Course{} = course, attrs) do
    course
    |> Course.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a course.

  ## Examples

      iex> delete_course(course)
      {:ok, %Course{}}

      iex> delete_course(course)
      {:error, %Ecto.Changeset{}}

  """
  def delete_course(%Course{} = course) do
    Repo.delete(course)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking course changes.

  ## Examples

      iex> change_course(course)
      %Ecto.Changeset{data: %Course{}}

  """
  def change_course(%Course{} = course, attrs \\ %{}) do
    Course.changeset(course, attrs)
  end

  alias StudyGarden.Courses.CourseSession

  @doc """
  Returns the list of course_sessions.

  ## Examples

      iex> list_course_sessions()
      [%CourseSession{}, ...]

  """
  def list_course_sessions do
    Repo.all(CourseSession)
  end

  @doc """
  Gets a single course_session.

  Raises `Ecto.NoResultsError` if the Course session does not exist.

  ## Examples

      iex> get_course_session!(123)
      %CourseSession{}

      iex> get_course_session!(456)
      ** (Ecto.NoResultsError)

  """
  def get_course_session!(id), do: Repo.get!(CourseSession, id)

  @doc """
  Gets the sessions for a course.

  Raises `Ecto.NoResultsError` if the course session does not exist.

  ## Examples

      iex> get_course_sessions_by_course!(123)
      [%CourseSession{}, ...]

  """
  def get_course_sessions_by_course!(course_id) do
    query =
      from CourseSession,
        where: [course_id: ^course_id],
        preload: [:course]

    Repo.all(query)
  end

  @doc """
  Creates a course_session.

  ## Examples

      iex> create_course_session(%{field: value})
      {:ok, %CourseSession{}}

      iex> create_course_session(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_course_session(attrs \\ %{}) do
    %CourseSession{}
    |> CourseSession.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a course_session.

  ## Examples

      iex> update_course_session(course_session, %{field: new_value})
      {:ok, %CourseSession{}}

      iex> update_course_session(course_session, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_course_session(%CourseSession{} = course_session, attrs) do
    course_session
    |> CourseSession.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a course_session.

  ## Examples

      iex> delete_course_session(course_session)
      {:ok, %CourseSession{}}

      iex> delete_course_session(course_session)
      {:error, %Ecto.Changeset{}}

  """
  def delete_course_session(%CourseSession{} = course_session) do
    Repo.delete(course_session)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking course_session changes.

  ## Examples

      iex> change_course_session(course_session)
      %Ecto.Changeset{data: %CourseSession{}}

  """
  def change_course_session(%CourseSession{} = course_session, attrs \\ %{}) do
    CourseSession.changeset(course_session, attrs)
  end

  alias StudyGarden.Courses.CourseSignUp

  @doc """
  Returns the list of course_sign_ups.

  ## Examples

      iex> list_course_sign_ups()
      [%CourseSignUp{}, ...]

  """
  def list_course_sign_ups do
    course_sign_up_query =
      from s in CourseSignUp,
        join: c in Course,
        on: s.course_id == c.id,
        order_by: c.title

    Repo.all(CourseSignUp) |> Repo.preload(:course)
  end

  @doc """
  Gets a single course_sign_up.

  Raises `Ecto.NoResultsError` if the Course sign up does not exist.

  ## Examples

      iex> get_course_sign_up!(123)
      %CourseSignUp{}

      iex> get_course_sign_up!(456)
      ** (Ecto.NoResultsError)

  """
  def get_course_sign_up!(id), do: Repo.get!(CourseSignUp, id)

  @doc """
  Gets a single course_sign_up.

  Raises `Ecto.NoResultsError` if the Course sign up does not exist.

  ## Examples

      iex> get_course_sign_up_by_stripe_checkout_session_id!(123)
      %CourseSignUp{}

      iex> get_course_sign_up_by_stripe_checkout_session_id!(456)
      ** (Ecto.NoResultsError)

  """
  def get_course_sign_up_by_stripe_checkout_session_id!(id) do
    Repo.get_by!(CourseSignUp, stripe_checkout_session_id: id) |> Repo.preload(:course)
  end

  @doc """
  Creates a course_sign_up.

  ## Examples

      iex> create_course_sign_up(%{field: value})
      {:ok, %CourseSignUp{}}

      iex> create_course_sign_up(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_course_sign_up(attrs \\ %{}) do
    %CourseSignUp{}
    |> CourseSignUp.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a course_sign_up.

  ## Examples

      iex> update_course_sign_up(course_sign_up, %{field: new_value})
      {:ok, %CourseSignUp{}}

      iex> update_course_sign_up(course_sign_up, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_course_sign_up(%CourseSignUp{} = course_sign_up, attrs) do
    course_sign_up
    |> CourseSignUp.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a course_sign_up.

  ## Examples

      iex> delete_course_sign_up(course_sign_up)
      {:ok, %CourseSignUp{}}

      iex> delete_course_sign_up(course_sign_up)
      {:error, %Ecto.Changeset{}}

  """
  def delete_course_sign_up(%CourseSignUp{} = course_sign_up) do
    Repo.delete(course_sign_up)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking course_sign_up changes.

  ## Examples

      iex> change_course_sign_up(course_sign_up)
      %Ecto.Changeset{data: %CourseSignUp{}}

  """
  def change_course_sign_up(%CourseSignUp{} = course_sign_up, attrs \\ %{}) do
    CourseSignUp.changeset(course_sign_up, attrs)
  end

  @doc ~S"""
  Delivers a course sign up success email.

  ## Examples

      iex> deliver_course_sign_up_success_email(email, course)
      {:ok, %{to: ..., body: ...}}

  """
  def deliver_course_sign_up_success_email(
        email,
        %Course{} = course,
        %CourseSignUp{} = course_sign_up
      ) do
    CourseSignUpNotifier.deliver_sign_up_success(email, course, course_sign_up)
  end
end

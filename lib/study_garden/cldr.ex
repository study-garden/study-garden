defmodule StudyGarden.Cldr do
  @moduledoc """
  Define a backend module that will host our Cldr configuration and public API.

  Most function calls in Cldr will be calls to functions on this module.
  """
  @default_locale "en"

  use Cldr,
    otp_app: :study_garden,
    providers: [
      Cldr.Number,
      Cldr.DateTime,
      Cldr.Routes,
      Money
    ],
    default_locale: @default_locale,
    gettext: StudyGardenWeb.Gettext
end

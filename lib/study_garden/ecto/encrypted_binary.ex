defmodule StudyGarden.Ecto.Encrypted.Binary do
  @moduledoc """
  Encrypted binary Ecto type.
  """

  use Cloak.Ecto.Binary, vault: StudyGarden.Cloak.Vault
end

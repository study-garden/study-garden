defmodule StudyGarden.Courses.CourseSignUp do
  use StudyGarden.Schema
  import Ecto.Changeset

  schema "course_sign_ups" do
    field :name, :string
    field :email, :string
    field :discord_username, :string
    field :payment_option, Ecto.Enum, values: [:full, :deposit]
    field :sg_stripe_key, :binary_id
    field :stripe_checkout_session_id, :string

    belongs_to :course, StudyGarden.Courses.Course

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(course_sign_up, attrs) do
    course_sign_up
    |> cast(attrs, [
      :name,
      :email,
      :discord_username,
      :payment_option,
      :course_id,
      :sg_stripe_key,
      :stripe_checkout_session_id
    ])
    |> validate_required([:name, :email, :discord_username, :payment_option, :course_id])
  end
end

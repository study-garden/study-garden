defmodule StudyGarden.Courses.CourseSession do
  @moduledoc """
  Course session schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "course_sessions" do
    field :start, :utc_datetime
    field :end, :utc_datetime
    field :zoom_url, :string
    field :course_id, :binary_id

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(course_session, attrs) do
    course_session
    |> cast(attrs, [:start, :end, :zoom_url])
    |> validate_required([:start, :end])
  end
end

defmodule StudyGarden.Courses.Course do
  @moduledoc """
  Course schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  @default_currency :USD
  @today Date.utc_today()

  @derive {Phoenix.Param, key: :slug}
  schema "courses" do
    field :description, :string
    field :title, :string
    field :subtitle, :string
    field :start_date, :date, default: @today
    field :end_date, :date, default: Date.add(@today, 90)
    field :tuition, Money.Ecto.Composite.Type, default_currency: @default_currency
    field :deposit, Money.Ecto.Composite.Type, default_currency: @default_currency
    field :stripe_tuition_price_id, :string
    field :stripe_deposit_price_id, :string
    field :slug, :string

    has_many :sessions, StudyGarden.Courses.CourseSession

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(course, attrs) do
    course
    |> cast(attrs, [
      :title,
      :subtitle,
      :description,
      :start_date,
      :end_date,
      :tuition,
      :deposit,
      :slug,
      :stripe_tuition_price_id,
      :stripe_deposit_price_id
    ])
    |> validate_required([
      :title,
      :start_date,
      :end_date,
      :tuition,
      :slug
    ])
    |> unique_constraint(:slug)
  end
end

defmodule StudyGarden.Courses.CourseSignUpNotifier do
  import Swoosh.Email

  alias StudyGarden.Mailer
  alias StudyGarden.Schools

  # Delivers the email using the application mailer.
  defp deliver(recipient, subject_content, body) do
    school = Schools.get_school!()
    subject = school.short_name <> "Study Garden - " <> subject_content

    email =
      new()
      |> to(recipient)
      |> from({school.full_name, "noreply@mimbres.study.garden"})
      |> subject(subject)
      |> text_body(body)

    with email_map <- StudyGarden.Mailer.to_map(email),
         {:ok, _job} <- enqueue_worker(email_map) do
      {:ok, email}
    end
  end

  defp enqueue_worker(email) do
    %{email: email}
    |> StudyGarden.Oban.Workers.SendEmail.new()
    |> Oban.insert()
  end

  def deliver_sign_up_success(user, course, course_sign_up) do
    school = Schools.get_school!()

    start_date_formatted = Calendar.strftime(course.start_date, "%A, %B %d, %Y")

    start_time_formatted =
      course.start_date
      |> Timex.to_datetime("America/Phoenix")
      |> Timex.format!("{h12} {am} ({Zname})")

    deliver(user.email, "You've successfully enrolled in #{course.title}!", """

    ==============================

    Hi #{user.email},

    You've successfully enrolled in #{course}!

    The first course session is on #{start_date_formatted} and starts at #{start_time_formatted}. Note that #{school.short_name} runs on the #{school.timezone} timezone.

    If you paid a deposit, we will send a reminder email to pay the remaining tuition 30 days from the day of the first session.

    From,
    #{school.full_name}

    ==============================
    """)
  end
end

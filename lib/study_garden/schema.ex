defmodule StudyGarden.Schema do
  @moduledoc """
  Module for base schema.

  Configure to use UUIDs and datetimes in milliseconds, and to rename the
  `inserted_at` field.
  """

  defmacro __using__(_opts) do
    quote do
      use Ecto.Schema

      @primary_key {:id, :binary_id, autogenerate: true}
      @foreign_key_type :binary_id
      @timestamps_opts [type: :utc_datetime_usec, inserted_at: :created_at]
    end
  end
end

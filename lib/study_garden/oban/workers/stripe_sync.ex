defmodule StudyGarden.Oban.Workers.StripeSyncWorker do
  require Logger

  use Oban.Worker
  alias StudyGarden.Stripe

  @impl Oban.Worker
  def perform(_job) do
    Logger.info("Syncing Stripe customers")
    Stripe.sync_stripe_customers()
  end
end

defmodule StudyGarden.Stripe.StripeProduct do
  @moduledoc """
  Stripe product schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "stripe_products" do
    field :stripe_id, :string
    field :stripe_name, :string
    field :stripe_active, :boolean, default: false
    field :default_price, :string
    field :stripe_metadata, :map
    field :stripe_created, :integer
    field :stripe_updated, :integer
    field :stripe_url, :string

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(stripe_product, attrs) do
    stripe_product
    |> cast(attrs, [:stripe_id, :stripe_name, :stripe_active, :default_price, :stripe_metadata, :stripe_created, :stripe_updated, :stripe_url])
    |> validate_required([:stripe_id, :stripe_name, :stripe_active, :stripe_created, :stripe_updated])
    |> unique_constraint(:stripe_id)
  end
end

defmodule StudyGarden.Stripe.StripeWebhookEvent do
  @moduledoc """
  Stripe webhook event schema. Currently only for incoming webhooks.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "stripe_webhook_events" do
    field :host, :string
    field :stripe_id, :string
    field :stripe_request_id, :string
    field :stripe_type, :string
    field :stripe_created, :integer
    field :stripe_api_version, :string
    field :stripe_idempotency_key, :string
    field :stripe_signature, :string

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(stripe_webhook_event, attrs) do
    stripe_webhook_event
    |> cast(attrs, [
      :stripe_id,
      :stripe_type,
      :host,
      :stripe_created,
      :stripe_api_version,
      :stripe_signature
    ])
    |> validate_required([
      :stripe_id,
      :stripe_request_id,
      :stripe_type,
      :host,
      :stripe_created,
      :stripe_api_version,
      :stripe_idempotency_key,
      :stripe_signature
    ])
    |> unique_constraint([:stripe_id])
  end
end

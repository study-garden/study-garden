defmodule StudyGarden.Stripe.StripeSubscription do
  use StudyGarden.Schema
  import Ecto.Changeset

  schema "stripe_subscriptions" do
    field :stripe_id, :string
    field :stripe_cancel_at_period_end, :boolean, default: false
    field :stripe_current_period_start, :integer
    field :stripe_current_period_end, :integer
    field :stripe_ended_at, :integer
    field :stripe_customer, :string
    field :stripe_items, :map
    field :stripe_latest_invoice, :string
    field :stripe_metadata, :map

    field :stripe_status, Ecto.Enum,
      values: [
        :incomplete,
        :incomplete_expired,
        :trialing,
        :active,
        :past_due,
        :canceled,
        :unpaid,
        :paused
      ]

    field :stripe_cancel_at, :integer
    field :stripe_canceled_at, :integer
    field :stripe_created, :integer
    field :stripe_days_until_due, :integer
    field :stripe_start_date, :integer
    field :sg_stripe_key, :binary_id

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(stripe_subscription, attrs) do
    stripe_subscription
    |> cast(attrs, [
      :stripe_id,
      :stripe_cancel_at_period_end,
      :stripe_current_period_start,
      :stripe_current_period_end,
      :stripe_ended_at,
      :stripe_customer,
      :stripe_items,
      :stripe_latest_invoice,
      :stripe_metadata,
      :stripe_status,
      :stripe_cancel_at,
      :stripe_canceled_at,
      :stripe_created,
      :stripe_days_until_due,
      :stripe_start_date,
      :sg_stripe_key
    ])
    |> validate_required([
      :stripe_id,
      :stripe_cancel_at_period_end,
      :stripe_current_period_start,
      :stripe_current_period_end,
      :stripe_customer,
      :stripe_latest_invoice,
      :stripe_status,
      :stripe_created,
      :stripe_start_date,
      :sg_stripe_key
    ])
    |> unique_constraint(:stripe_id)
  end
end

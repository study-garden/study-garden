defmodule StudyGarden.Stripe.StripeSubscriptionNotifier do
  import Swoosh.Email
  alias StudyGarden.Mailer

  def deliver_subscription_success(%{name: name, email: email}) do
    new()
    |> to({name, email})
    |> from({"Phoenix Team", "team@example.com"})
    |> subject("Welcome to Phoenix, #{name}!")
    |> html_body("<h1>Hello, #{name}</h1>")
    |> text_body("Hello, #{name}\n")
    |> Mailer.deliver()
  end
end

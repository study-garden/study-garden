defmodule StudyGarden.Stripe.StripeCustomer do
  @moduledoc """
  Stripe customer schema. Only store the Stripe ID for privacy and security.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "stripe_customers" do
    field :stripe_id, :string

    belongs_to :user, StudyGarden.Identities.User

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(stripe_customer, attrs) do
    stripe_customer
    |> cast(attrs, [:stripe_id])
    |> validate_required([:stripe_id])
    |> unique_constraint(:stripe_id)
  end
end

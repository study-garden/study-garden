defmodule StudyGarden.Stripe.WebhookEventHandler do
  @moduledoc """
  Stripe webhook event handler. Webhook events are categorized by type (e.g. `payment_intent.succeeded`, `invoice.paid`, etc.)
  """

  def handle_event(%Stripe.Event{type: "payment_intent.succeeded"} = event) do
    IO.inspect(event, label: "Stripe event handler")
  end

  def handle_event(%Stripe.Event{type: "customer.subscription.created"} = event) do
  end

  def handle_event(%Stripe.Event{} = event) do
    IO.inspect(event, label: "Stripe event handler")
  end
end

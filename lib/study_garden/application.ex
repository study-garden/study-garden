defmodule StudyGarden.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    # Logging for Oban
    Oban.Telemetry.attach_default_logger(encode: false, level: :debug)

    children = [
      StudyGardenWeb.Telemetry,
      StudyGarden.Repo,
      {Oban, Application.fetch_env!(:study_garden, Oban)},
      {DNSCluster, query: Application.get_env(:study_garden, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: StudyGarden.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: StudyGarden.Finch},
      StudyGarden.Cloak.Vault,
      Cldr.Currency,
      StudyGarden.Discord.Consumer,

      # Start a worker by calling: StudyGarden.Worker.start_link(arg)
      # {StudyGarden.Worker, arg},
      # Start to serve requests, typically the last entry
      StudyGardenWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: StudyGarden.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    StudyGardenWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

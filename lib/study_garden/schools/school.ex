defmodule StudyGarden.Schools.School do
  @moduledoc """
  School schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  @primary_key {:id, :boolean, autogenerate: false}
  @foreign_key_type :binary_id
  schema "schools" do
    field :full_name, :string
    field :short_name, :string
    field :description, :string
    field :timezone, :string
    field :stripe_customer_portal_url, :string
    field :google_calendar_embed_url, :string

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(school, attrs) do
    school
    |> cast(attrs, [
      :full_name,
      :short_name,
      :description,
      :timezone,
      :stripe_customer_portal_url,
      :google_calendar_embed_url
    ])
    |> validate_required([:full_name, :short_name, :description, :timezone])
    |> validate_length(:description, min: 20)
    # This checks that one and only school exists in the schools table.
    # The checked field or fields don't really matter for this purpose.
    |> unique_constraint([:short_name], name: :schools_pkey)
  end
end

defmodule StudyGarden.Schools.Announcement do
  @moduledoc """
  School announcement schema.
  """

  use StudyGarden.Schema
  import Ecto.Changeset

  schema "school_announcements" do
    field :message, :string
    field :pinned, :boolean, default: false

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(announcement, attrs) do
    announcement
    |> cast(attrs, [:pinned, :message])
    |> validate_required([:pinned, :message])
  end
end

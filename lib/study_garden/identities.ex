defmodule StudyGarden.Identities do
  @moduledoc """
  The Identities context.
  """

  require Logger

  import Ecto.Query, warn: false

  alias StudyGarden.Repo

  alias StudyGarden.Identities.{User, UserToken, UserNotifier, DiscordUser}

  ## Database getters

  def get_user_by_email_token(token, context) do
    with {:ok, query} <- UserToken.verify_email_token_query(token, context),
         %User{} = user <- Repo.one(query) |> Repo.preload(:discord_user) do
      user
    else
      _ -> nil
    end
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User) |> Repo.preload(:discord_user)
  end

  @doc """
  Gets a user by email.

  ## Examples

      iex> get_user_by_email("foo@example.com")
      %User{}

      iex> get_user_by_email("unknown@example.com")
      nil

  """
  def get_user_by_email(email) when is_binary(email) do
    Repo.get_by(User, email: email) |> Repo.preload([:discord_user, roles: [:role]])
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id) |> Repo.preload([:discord_user, roles: [:role]])

  ## User registration

  @doc """
  Registers a user.

  ## Examples

      iex> register_user(%{field: value})
      {:ok, %User{}}

      iex> register_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def register_user(attrs) do
    Logger.info("Registering new user")

    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  ## Settings

  @doc """
  Returns an `%Ecto.Changeset{}` for changing the user email.

  ## Examples

      iex> change_user_email(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user_email(user, attrs \\ %{}) do
    User.email_changeset(user, attrs, validate_email: false)
  end

  @doc """
  Emulates that the email will change without actually changing
  it in the database.

  ## Examples

      iex> apply_user_email(user, "valid password", %{email: ...})
      {:ok, %User{}}

      iex> apply_user_email(user, "invalid password", %{email: ...})
      {:error, %Ecto.Changeset{}}

  """
  def apply_user_email(user, attrs) do
    user
    |> User.email_changeset(attrs)
    |> Ecto.Changeset.apply_action(:update)
  end

  @doc """
  Updates the user email using the given token.

  If the token matches, the user email is updated and the token is deleted.
  The confirmed_at date is also updated to the current time.
  """
  def update_user_email(user, token) do
    context = "change:#{user.email}"

    with {:ok, query} <- UserToken.verify_change_email_token_query(token, context),
         %UserToken{sent_to: email} <- Repo.one(query),
         {:ok, _} <- Repo.transaction(user_email_multi(user, email, context)) do
      :ok
    else
      _ -> :error
    end
  end

  defp user_email_multi(user, email, context) do
    changeset =
      user
      |> User.email_changeset(%{email: email})
      |> User.confirm_changeset()

    Ecto.Multi.new()
    |> Ecto.Multi.update(:user, changeset)
    |> Ecto.Multi.delete_all(:tokens, UserToken.by_user_and_contexts_query(user, [context]))
  end

  @doc ~S"""
  Delivers the update email instructions to the given user.

  ## Examples

      iex> deliver_user_update_email_instructions(user, current_email, &url(~p"/users/settings/confirm_email/#{&1})")
      {:ok, %{to: ..., body: ...}}

  """
  def deliver_user_update_email_instructions(%User{} = user, current_email, update_email_url_fun)
      when is_function(update_email_url_fun, 1) do
    {encoded_token, user_token} = UserToken.build_email_token(user, "change:#{current_email}")

    Repo.insert!(user_token)
    UserNotifier.deliver_update_email_instructions(user, update_email_url_fun.(encoded_token))
  end

  ## Session

  @doc """
  Generates a session token.
  """
  def generate_user_session_token(user) do
    {token, user_token} = UserToken.build_session_token(user)
    Repo.insert!(user_token)
    token
  end

  @doc """
  Gets the user with the given signed token.
  """
  def get_user_by_session_token(token) do
    {:ok, query} = UserToken.verify_session_token_query(token)
    Repo.one(query) |> Repo.preload([:discord_user, roles: [:role]])
  end

  @doc """
  Deletes the signed token with the given context.
  """
  def delete_user_session_token(token) do
    Repo.delete_all(UserToken.by_token_and_context_query(token, "session"))
    :ok
  end

  ## Confirmation

  @doc ~S"""
  Delivers the confirmation email instructions to the given user.

  ## Examples

      iex> deliver_user_confirmation_instructions(user, &url(~p"/users/confirm/#{&1}"))
      {:ok, %{to: ..., body: ...}}

      iex> deliver_user_confirmation_instructions(confirmed_user, &url(~p"/users/confirm/#{&1}"))
      {:error, :already_confirmed}

  """
  def deliver_user_confirmation_instructions(%User{} = user, confirmation_url_fun)
      when is_function(confirmation_url_fun, 1) do
    if user.confirmed_at do
      {:error, :already_confirmed}
    else
      {encoded_token, user_token} = UserToken.build_email_token(user, "confirm")
      Repo.insert!(user_token)
      UserNotifier.deliver_confirmation_instructions(user, confirmation_url_fun.(encoded_token))
    end
  end

  @doc """
  Confirms a user. Does nothing if they're already confirmed.
  """
  def confirm_user(%User{confirmed_at: confirmed_at} = user) when is_nil(confirmed_at) do
    user
    |> User.confirm_changeset()
    |> Repo.update()
  end

  def confirm_user(%User{confirmed_at: confirmed_at} = user) when not is_nil(confirmed_at) do
    {:ok, user}
  end

  defp confirm_user_multi(user) do
    Ecto.Multi.new()
    |> Ecto.Multi.update(:user, User.confirm_changeset(user))
    |> Ecto.Multi.delete_all(:tokens, UserToken.by_user_and_contexts_query(user, ["confirm"]))
  end

  ## Authentication
  def login_or_register_user(email) do
    case get_user_by_email(email) do
      # Found existing user.
      %User{} = user ->
        {email_token, token} = UserToken.build_email_token(user, "email_login_link")
        Repo.insert!(token)

        UserNotifier.deliver_login_link(
          user,
          "#{StudyGardenWeb.Endpoint.url()}/users/log_in/email/token/#{email_token}"
        )

      # New user, create a new account.
      _ ->
        {:ok, user} = register_user(%{email: email})

        {email_token, token} = UserToken.build_email_token(user, "email_login_link")
        Repo.insert!(token)

        UserNotifier.deliver_registration_link(
          user,
          "#{StudyGardenWeb.Endpoint.url()}/users/log_in/email/token/#{email_token}"
        )
    end
  end

  ## Examples
  @doc """
  Registers a user from Discord OAuth info.

  ## Examples

      iex> register_user_from_discord(%Ueberauth.Auth%{field: value})
      {:ok, %User{}}

      iex> register_user_from_discord(%Ueberauth.Auth%{field: bad_value})
      {:error, reason}

  """
  def register_user_from_discord(%Ueberauth.Auth{provider: :discord} = auth_data) do
    Logger.info("Registering new user through Discord OAuth")

    extra_data = auth_data.extra.raw_info
    user_data = extra_data.user

    # Set the `confirmed_at` time in the User schema if the Discord account is verified.
    confirmed_at_time =
      if user_data["verified"] do
        DateTime.utc_now()
      else
        nil
      end

    user_params = %{
      email: user_data["email"],
      confirmed_at: confirmed_at_time
    }

    Logger.debug("OAuth Discord user params")

    discord_user_params = %{
      discord_id: user_data["id"],
      discord_username: user_data["username"],
      discord_email: user_data["email"],
      access_token: auth_data.credentials.token,
      refresh_token: auth_data.credentials.refresh_token,
      access_token_expires_at: auth_data.credentials.expires_at,
      discord_verified: user_data["verified"],
      user: user_params
    }

    Logger.debug(discord_user_params)

    result =
      %DiscordUser{}
      |> DiscordUser.changeset(discord_user_params)
      |> Repo.insert(
        on_conflict: [
          set: [
            discord_username: discord_user_params.discord_username,
            discord_email: discord_user_params.discord_email,
            discord_verified: discord_user_params.discord_verified,
            access_token: discord_user_params.access_token,
            access_token_expires_at: discord_user_params.access_token_expires_at,
            refresh_token: discord_user_params.refresh_token
          ]
        ],
        conflict_target: :discord_id,
        returning: true
      )

    case result do
      {:ok, discord_user} ->
        Logger.info("New user successfully registered through Discord OAuth")
        Logger.info("Discord user: #{discord_user.discord_username}")
        Logger.info("Discord email: #{discord_user.discord_email}")
        UserNotifier.deliver_discord_oauth_success(discord_user.user, discord_user)
        {:ok, discord_user.user}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Gets a single Discord user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_discord_user!(123)
      %User{}

      iex> get_discord_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_discord_user!(id), do: Repo.get!(DiscordUser, id)

  @doc """
  Gets a single Discord user by email.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_discord_user!(123)
      %User{}

      iex> get_discord_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_discord_user_by_email(id) do
    Repo.get_by(DiscordUser, id)
  end

  @doc """
  Returns the list of Discord users.

  ## Examples

      iex> list_discord_users()
      [%DiscordUser{}, ...]

  """
  def list_discord_users do
    Repo.all(DiscordUser) |> Repo.preload(:user)
  end

  def user_is_admin?(user_id) do
    user = get_user!(user_id)

    Enum.any?(user.roles, fn role -> role.role.name == "admin" end)
  end
end

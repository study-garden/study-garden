defmodule StudyGarden.Cloak.Vault do
  @moduledoc """
  Cloak vault for data encryption.
  """

  use Cloak.Vault, otp_app: :study_garden

  @impl GenServer
  def init(config) do
    config =
      Keyword.put(config, :ciphers,
        default:
          {Cloak.Ciphers.AES.GCM, tag: "AES.GCM.V1", key: decode_env!("CLOAK_ENCRYPTION_KEY")}
      )

    {:ok, config}
  end

  defp decode_env!(var) when is_binary(var) do
    var
    |> System.get_env()
    |> Base.decode64!()
  end
end

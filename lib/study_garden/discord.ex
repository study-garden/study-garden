defmodule StudyGarden.Discord do
  @moduledoc """
  The Discord context.
  """
  require Logger

  import Ecto.Query, warn: false

  alias Nostrum.Api

  alias StudyGarden.Repo

  alias StudyGarden.Discord.GuildRole

  @doc """
  Returns the list of discord_guild_roles.

  ## Examples

      iex> list_discord_guild_roles()
      [%GuildRole{}, ...]

  """
  def list_discord_guild_roles do
    Repo.all(GuildRole)
  end

  @doc """
  Gets a single guild_role.

  Raises `Ecto.NoResultsError` if the Guild role does not exist.

  ## Examples

      iex> get_guild_role!(123)
      %GuildRole{}

      iex> get_guild_role!(456)
      ** (Ecto.NoResultsError)

  """
  def get_guild_role!(id), do: Repo.get!(GuildRole, id)

  @doc """
  Gets a single guild role by Discord ID.

  Raises `Ecto.NoResultsError` if the Guild role does not exist.

  ## Examples

      iex> get_guild_role_by_discord_id!(123)
      %GuildRole{}

      iex> get_guild_role_by_discord_id!(456)
      ** (Ecto.NoResultsError)

  """
  def get_guild_role_by_discord_id!(discord_id),
    do: Repo.get_by!(GuildRole, discord_id: discord_id)

  @doc """
  Creates a guild_role.

  ## Examples

      iex> create_guild_role(%{field: value})
      {:ok, %GuildRole{}}

      iex> create_guild_role(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_guild_role(attrs \\ %{}) do
    %GuildRole{}
    |> GuildRole.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a guild_role.

  ## Examples

      iex> update_guild_role(guild_role, %{field: new_value})
      {:ok, %GuildRole{}}

      iex> update_guild_role(guild_role, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_guild_role(%GuildRole{} = guild_role, attrs) do
    guild_role
    |> GuildRole.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a guild_role.

  ## Examples

      iex> delete_guild_role(guild_role)
      {:ok, %GuildRole{}}

      iex> delete_guild_role(guild_role)
      {:error, %Ecto.Changeset{}}

  """
  def delete_guild_role(%GuildRole{} = guild_role) do
    Repo.delete(guild_role)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking guild_role changes.

  ## Examples

      iex> change_guild_role(guild_role)
      %Ecto.Changeset{data: %GuildRole{}}

  """
  def change_guild_role(%GuildRole{} = guild_role, attrs \\ %{}) do
    GuildRole.changeset(guild_role, attrs)
  end

  @doc """
  Returns the list of Discord guild roles from Discord.

  ## Examples

      iex> list_discord_guild_roles_from_discord()
      [%Nostrum.Struct.Guild.Role{}, ...]

  """
  def list_guild_roles_from_discord do
    Api.get_guild_roles!(System.get_env("DISCORD_GUILD_ID"))
  end

  @doc """
  Syncs guild role from Discord to the database.

  ## Examples

      iex> sync_guild_role_from_discord(role)
      {:ok, %GuildRole{}}

      iex> sync_guild_role_from_discord(bad_role)
      {:error, %Ecto.Changeset{}}

  """
  def sync_guild_role_from_discord(%Nostrum.Struct.Guild.Role{} = role) do
    Logger.debug("Syncing Discord guild role \"#{role.name}\" (#{role.id}) to database")

    attrs = %{
      discord_id: Integer.to_string(role.id),
      discord_name: role.name,
      discord_position: role.position,
      discord_color: role.color
    }

    %GuildRole{}
    |> GuildRole.changeset(attrs)
    |> Repo.insert(
      on_conflict: {:replace_all_except, [:id, :created_at]},
      conflict_target: :discord_id,
      returning: true
    )
  end

  @doc """
  Syncs guild roles from Discord to the database.

  ## Examples

      iex> sync_guild_role_from_discord(role)
      {:ok, %GuildRole{}}

      iex> sync_guild_role_from_discord(bad_role)
      {:error, %Ecto.Changeset{}}

  """
  def sync_guild_roles_from_discord() do
    Logger.info("Syncing Discord guild roles to database")
    roles = list_guild_roles_from_discord()
    Enum.each(roles, fn role -> sync_guild_role_from_discord(role) end)
  end

  def list_discord_guild_members_from_discord!() do
    Nostrum.Api.list_guild_members!(String.to_integer(System.get_env("DISCORD_GUILD_ID")),
      limit: 1000
    )
  end
end

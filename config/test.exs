import Config

# Only in tests, remove the complexity from the password hashing algorithm
config :argon2_elixir, t_cost: 1, m_cost: 8

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :study_garden, StudyGarden.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  database:
    System.get_env("POSTGRES_DB") || "study_garden_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: System.schedulers_online() * 2

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :study_garden, StudyGardenWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "eEHl8rDI5ILoZdX5tPJ3ptE5Ad+mDsMbnw1S0f49+7Rh13JfHnJFgcT1Ubok0iZk",
  server: false

# In test we don't send emails.
config :study_garden, StudyGarden.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# Prevent Oban from running jobs and plugins during test runs
config :study_garden, Oban, testing: :inline

# Set a Cloak vault encryption key
config :study_garden, StudyGarden.Cloak.Vault,
  ciphers: [
    default:
      {Cloak.Ciphers.AES.GCM,
       tag: "AES.GCM.V1", key: Base.decode64!("9Xw2FIE3y3OcmxMWR4oak3vh2ApFAvLIUZ3c2few7tQ=")}
  ]

# Argon2 configuration. Lower setting values for testing.
config :argon2_elixir,
  t_cost: 1,
  m_cost: 8

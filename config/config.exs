# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :study_garden,
  ecto_repos: [StudyGarden.Repo],
  generators: [timestamp_type: :utc_datetime_usec, binary_id: true]

config :study_garden, StudyGarden.Repo,
  migration_primary_key: [name: :id, type: :binary_id],
  migration_timestamps: [
    type: :utc_datetime_usec,
    inserted_at: :created_at
  ]

# Configures the endpoint
config :study_garden, StudyGardenWeb.Endpoint,
  url: [host: "localhost"],
  adapter: Bandit.PhoenixAdapter,
  render_errors: [
    formats: [html: StudyGardenWeb.ErrorHTML, json: StudyGardenWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: StudyGarden.PubSub,
  live_view: [signing_salt: "zmAbXIqb"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :study_garden, StudyGarden.Mailer, adapter: Swoosh.Adapters.Local

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.17.11",
  study_garden: [
    args:
      ~w(js/app.ts js/storybook.ts --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.4.0",
  study_garden: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ],
  storybook: [
    args: ~w(
          --config=tailwind.config.js
          --input=css/storybook.css
          --output=../priv/static/assets/storybook.css
        ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [
    :request_id,
    # Discord-related metadata
    :shard,
    :guild,
    :channel
  ]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Oban configuration
config :study_garden, Oban,
  repo: StudyGarden.Repo,
  queues: [default: 10],
  plugins: [
    # Retain jobs for 31 days
    {Oban.Plugins.Pruner, max_age: 60 * 60 * 24 * 31},
    # Rescue orphan jobs after 30 minutes of a deployment or node restart
    {Oban.Plugins.Lifeline, rescue_after: :timer.minutes(30)},
    {Oban.Plugins.Cron,
     crontab: [
       {"5 * * * *", StudyGarden.Oban.Workers.StripeSyncWorker}
     ]}
  ]

# Cldr configuration
config :study_garden, StudyGarden.Cldr, locales: ["en"]

# Configures `ex_money`
config :ex_money,
  default_cldr_backend: StudyGarden.Cldr,
  auto_start_exchange_rate_service: false

# Configures Nostrum, the Discord Elixir library
config :nostrum,
  token: System.get_env("DISCORD_BOT_TOKEN"),
  gateway_intents: [
    :guilds,
    :guild_members,
    :guild_presences,
    :guild_invites,
    :direct_messages
  ],
  request_guild_members: true

# Cloak configuration

cloak_encryption_key = System.get_env("CLOAK_ENCRYPTION_KEY")

if cloak_encryption_key do
  config :study_garden, StudyGarden.Cloak.Vault,
    ciphers: [
      default:
        {Cloak.Ciphers.AES.GCM,
         tag: "AES.GCM.V1", key: Base.decode64!(System.get_env("CLOAK_ENCRYPTION_KEY"))}
    ]
else
  config :study_garden, StudyGarden.Cloak.Vault,
    ciphers: [
      default: {Cloak.Ciphers.AES.GCM, tag: "AES.GCM.V1", key: ""}
    ]
end

# Ueberauth configuration
config :ueberauth, Ueberauth,
  # default is "/auth"
  base_path: "/oauth",
  providers: [
    discord: {Ueberauth.Strategy.Discord, [default_scope: "identify email guilds guilds.join"]}
  ]

config :ueberauth, Ueberauth.Strategy.Discord.OAuth,
  client_id: System.get_env("DISCORD_CLIENT_ID"),
  client_secret: System.get_env("DISCORD_CLIENT_SECRET")

# Stripe configuration
config :stripity_stripe, api_key: System.get_env("STRIPE_SECRET_KEY")

# Argon2 configuration. Ensure that the system doesn't run into out-of-memory
# issues by setting smaller values.
config :argon2_elixir,
  t_cost: 18,
  m_cost: 15

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"

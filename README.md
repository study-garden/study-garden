# Study Garden

Study Garden is an educational platform where scholars can give lectures, hold
discussions with students, and seek funding.

## Development

Follow the [Getting Started](#getting-started) guide to start contributing.

### Getting Started {#getting-started}

#### System Requirements
##### Elixir and Erlang

Ensure that you are installing Erlang with an OTP version of at least 25 and at least version 1.14 of Elixir. Current recommended versions are Erlang/OTP 26 and Elixir 1.16.

- [Install Erlang](https://elixir-lang.org/install.html#installing-erlang)
- [Install Elixir](https://elixir-lang.org/install.html#distributions)

###### asdf

You can also conveniently install Erlang and Elixir using [asdf](https://github.com/tabfugnic/asdf.el).

##### PostgreSQL
The project uses the PostgreSQL database. Install and run at least version 15 of PostgreSQL on your local environment. 

- [Install PostgreSQL](https://www.postgresql.org/download/)
- Check that the PostgreSQL server is up by running `postgres --version`
- Check that the `psql` PostgreSQL client is up by running `psql --version`

##### Distribution-Specific Libraries

On Linux, you may need to install [inotify-tools](https://github.com/inotify-tools/inotify-tools) if you want to watch source code files while running the development server and update them if you edit them. 

Installation can vary depending on your distribution so you will have to look up installation instructions.

#### Configuration

Once Elixir and Erlang are installed, run `mix` and it will prompt you to install the `hex` package manager. Run `mix setup` to install and setup dependencies. This will also create the `study_garden_dev` database and run the necessary database migrations.

Next, run `mix phx.gen.cert` to generate a self-signed SSL certificate for the use of local HTTPS.

#### Running the Development Server

Run `iex -S mix phx.server` to start the Phoenix development server in Elixir's (IEx)[https://hexdocs.pm/iex/1.16/IEx.html] interactive shell. 

After starting the development server, navigate to <https://localhost:4001>.

For insecure HTTP, navigate to <http://localhost:4000> instead.

You may get a security warning about insecure connections when interacting with the HTTPS server. You can ignore this warning and proceed. You may also encounter the following notice in the console log which you can ignore too:

```
[notice] TLS :server: In state :abbreviated received CLIENT ALERT: Fatal - Bad Certificate
```

### Frontend Development

#### Storybook

This project has a [Storybook](https://storybook.js.org/), a frontend development tool to display and test UI components and pages. Navigate to <https://localhost:4001/dev/storybook/> to check out the project's components and test pages.

### Testing

Run `mix test` to run tests.

#### Static Code Analysis with Credo

[Credo](https://github.com/rrrene/credo) is "is a static code analysis tool for the Elixir language with a focus on teaching and code consistency".

Run `mix credo` to check your code for any refactoring opportunities, code inconsistencies, and other mistakes.

#### Security Static Analysis with Sobelow

[Sobelow](https://github.com/nccgroup/sobelow) is "a security-focused static analysis tool for Elixir & the Phoenix framework".

Run `mix sobelow` to scan the project for common security vulnerabilities.

#### Stripe
To test Stripe integration and functionality such as making API requests or receiving webhook events, install the [Stripe CLI](https://stripe.com/docs/stripe-cli).

### Troubleshooting

#### PostgreSQL

If you are running any `mix ecto` tasks and you get an error like:

```
16:20:00.069 [error] Postgrex.Protocol (#PID<0.123.0>) failed to connect: ** (Postgrex.Error) FATAL 28P01 (invalid_password) password authentication failed for user "postgres"
```

You will need to set a password for the `postgres` role. This is distribution-specific, so please consult your distribution's documentation on setting up PostgreSQL.

You will also need to change the authentication method of your `localhost` records in `pg_hba.conf` to one that accepts passwords. Please consult PostgreSQL docs here: <https://www.postgresql.org/docs/current/auth-pg-hba-conf.html> 


#### CSS and Styling Issues

If you are having trouble with getting CSS styling to update, you may need to prune old, static assets with `mix phx.digest.clean --all`.

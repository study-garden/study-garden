// See the Tailwind configuration guide for advanced usage
// https://tailwindcss.com/docs/configuration

const plugin = require("tailwindcss/plugin");
const fs = require("fs");
const path = require("path");

module.exports = {
  content: [
    "./js/**/*.js",
    "../lib/study_garden_web.ex",
    "../lib/study_garden_web/**/*.*ex",
  ],
  prefix: "tw-",
  important: ".sg",
  theme: {
    extend: {
      colors: {
        transparent: "transparent",
        current: "currentColor",
        brand: "#3f8d00",
        green: {
          DEFAULT: "#5cc800",
          "5": "#f6fcf2",
          "10": "#e1f3d4",
          "20": "#bee6a4",
          "30": "#8cd365",
          "40": "#61bd34",
          "50": "#519a2d",
          "60": "#427726",
          "70": "#32571f",
          "80": "#263d19",
          "90": "#172010",
          "30-dull": "#375625",
          "60-dull": "#5c7055",
        },
        red: {
          DEFAULT: "#ff918f",
          "5": "#fff8f8",
          "10": "#ffe9e6",
          "20": "#ffcfcb",
          "30": "#ffaba6",
          "40": "#ff8481",
          "50": "#df6060",
          "60": "#ac4c4c",
          "70": "#7c3938",
          "80": "#562a29",
          "90": "#2c1817",
        },
        orange: {
          DEFAULT: "#ff9800",
          "5": "#fff9f2",
          "10": "#ffebd1",
          "20": "#ffd39b",
          "30": "#ffb14d",
          "40": "#ef9306",
          "50": "#c27810",
          "60": "#965e13",
          "70": "#6d4513",
          "80": "#4c3112",
          "90": "#281b0c",
        },
        yellow: {
          DEFAULT: "#c9b000",
          "5": "#fcfaf0",
          "10": "#f4efcc",
          "20": "#e7dc92",
          "30": "#d6c243",
          "40": "#c2a800",
          "50": "#9e8800",
          "60": "#7b6a00",
          "70": "#5a4d00",
          "80": "#403700",
          "90": "#221e00",
        },
        "pea-green": {
          DEFAULT: "#a3bc00",
          "5": "#f9fbf0",
          "10": "#ecf1cc",
          "20": "#d6e190",
          "30": "#b9cb40",
          "40": "#9cb300",
          "50": "#7f9200",
          "60": "#637100",
          "70": "#485300",
          "80": "#333b00",
          "90": "#1c2000",
        },
        jade: {
          DEFAULT: "#00c98a",
          "5": "#f3fcf6",
          "10": "#d5f5e1",
          "20": "#a3e9c0",
          "30": "#53d896",
          "40": "#0ec07a",
          "50": "#1a9c64",
          "60": "#1e794f",
          "70": "#1c583b",
          "80": "#193e2a",
          "90": "#122118",
        },
        cyan: {
          DEFAULT: "#00c5ba",
          "5": "#f2fcfa",
          "10": "#d4f4ef",
          "20": "#a0e7de",
          "30": "#46d5c7",
          "40": "#11bcaf",
          "50": "#1c998e",
          "60": "#1f776e",
          "70": "#1d5650",
          "80": "#193d39",
          "90": "#12201e",
        },
        blue: {
          DEFAULT: "#00bfee",
          "5": "#f4fbff",
          "10": "#daf1ff",
          "20": "#ade1ff",
          "30": "#60cdff",
          "40": "#10b5ed",
          "50": "#1f93bf",
          "60": "#227293",
          "70": "#20536a",
          "80": "#1c3a4a",
          "90": "#131f26",
        },
        indigo: {
          DEFAULT: "#88b0ff",
          "5": "#faf9ff",
          "10": "#ececff",
          "20": "#d7d6ff",
          "30": "#bcbaff",
          "40": "#a19eff",
          "50": "#827fd8",
          "60": "#6563a8",
          "70": "#4a487b",
          "80": "#343357",
          "90": "#1c1c2f",
        },
        purple: {
          DEFAULT: "#c69eff",
          "5": "#fcf9ff",
          "10": "#f6e9ff",
          "20": "#ead0ff",
          "30": "#dab0ff",
          "40": "#c890ff",
          "50": "#a472d7",
          "60": "#805aa5",
          "70": "#5d4277",
          "80": "#412f52",
          "90": "#221a29",
        },
        magenta: {
          DEFAULT: "#e78eff",
          "5": "#fff8fe",
          "10": "#fee7fa",
          "20": "#fdcbf4",
          "30": "#f8a6ed",
          "40": "#f27ee5",
          "50": "#c963be",
          "60": "#9b4e92",
          "70": "#6f3b69",
          "80": "#4d2b49",
          "90": "#281925",
        },
        cauliflower: {
          DEFAULT: "#ecf1eb",
        },
        discord: "#5865F2",
      },
      borderWidth: {
        DEFAULT: "1px",
        "0": "0",
        "1.5": "1.5px",
        "2": "2px",
        "3": "3px",
        "4": "4px",
        "5": "6px",
      },
      spacing: {
        "128": "32rem",
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    // Allows prefixing tailwind classes with LiveView classes to add rules
    // only when LiveView classes are applied, for example:
    //
    //     <div class="phx-click-loading:animate-ping">
    //
    plugin(({ addVariant }) =>
      addVariant("phx-no-feedback", [
        ".phx-no-feedback&",
        ".phx-no-feedback &",
      ]),
    ),
    plugin(({ addVariant }) =>
      addVariant("phx-click-loading", [
        ".phx-click-loading&",
        ".phx-click-loading &",
      ]),
    ),
    plugin(({ addVariant }) =>
      addVariant("phx-submit-loading", [
        ".phx-submit-loading&",
        ".phx-submit-loading &",
      ]),
    ),
    plugin(({ addVariant }) =>
      addVariant("phx-change-loading", [
        ".phx-change-loading&",
        ".phx-change-loading &",
      ]),
    ),

    // Embeds Heroicons (https://heroicons.com) into your app.css bundle
    // See your `CoreComponents.icon/1` for more information.
    //
    plugin(function ({ matchComponents, theme }) {
      let iconsDir = path.join(__dirname, "../deps/heroicons/optimized");
      let values = {};
      let icons = [
        ["", "/24/outline"],
        ["-solid", "/24/solid"],
        ["-mini", "/20/solid"],
        ["-micro", "/16/solid"],
      ];
      icons.forEach(([suffix, dir]) => {
        fs.readdirSync(path.join(iconsDir, dir)).forEach((file) => {
          let name = path.basename(file, ".svg") + suffix;
          values[name] = { name, fullPath: path.join(iconsDir, dir, file) };
        });
      });
      matchComponents(
        {
          hero: ({ name, fullPath }) => {
            let content = fs
              .readFileSync(fullPath)
              .toString()
              .replace(/\r?\n|\r/g, "");
            let size = theme("spacing.6");
            if (name.endsWith("-mini")) {
              size = theme("spacing.5");
            } else if (name.endsWith("-micro")) {
              size = theme("spacing.4");
            }
            return {
              [`--hero-${name}`]: `url('data:image/svg+xml;utf8,${content}')`,
              "-webkit-mask": `var(--hero-${name})`,
              mask: `var(--hero-${name})`,
              "mask-repeat": "no-repeat",
              "background-color": "currentColor",
              "vertical-align": "middle",
              display: "inline-block",
              width: size,
              height: size,
            };
          },
        },
        { values },
      );
    }),
  ],
};

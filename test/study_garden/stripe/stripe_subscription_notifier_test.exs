defmodule StudyGarden.Stripe.StripeSubscriptionNotifierTest do
  use ExUnit.Case, async: true
  import Swoosh.TestAssertions

  alias StudyGarden.Stripe.StripeSubscriptionNotifier

  test "deliver_subscription_success/1" do
    user = %{name: "Alice", email: "alice@example.com"}

    StripeSubscriptionNotifier.deliver_subscription_success(user)

    assert_email_sent(
      subject: "Welcome to Phoenix, Alice!",
      to: {"Alice", "alice@example.com"},
      text_body: ~r/Hello, Alice/
    )
  end
end

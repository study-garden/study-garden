defmodule StudyGarden.CoursesTest do
  use StudyGarden.DataCase

  alias StudyGarden.Courses

  describe "courses" do
    alias StudyGarden.Courses.Course

    import StudyGarden.CoursesFixtures

    @default_currency :USD
    @invalid_attrs %{
      description: nil,
      title: nil,
      subtitle: nil,
      start_date: nil,
      end_date: nil,
      tuition: nil,
      deposit: nil,
      slug: nil
    }

    test "list_courses/0 returns all courses" do
      course = course_fixture()
      assert Courses.list_courses() == [course]
    end

    test "get_course!/1 returns the course with given id" do
      course = course_fixture()
      assert Courses.get_course!(course.id) == course
    end

    test "create_course/1 with valid data creates a course" do
      valid_attrs = %{
        description: "some description",
        title: "some title",
        subtitle: "some subtitle",
        start_date: ~D[2024-04-02],
        end_date: ~D[2024-04-02],
        tuition: "120.5",
        deposit: "120.5",
        slug: "some slug"
      }

      assert {:ok, %Course{} = course} = Courses.create_course(valid_attrs)
      assert course.description == "some description"
      assert course.title == "some title"
      assert course.subtitle == "some subtitle"
      assert course.start_date == ~D[2024-04-02]
      assert course.end_date == ~D[2024-04-02]
      assert course.tuition == Money.new(@default_currency, "120.5")
      assert course.deposit == Money.new(@default_currency, "120.5")
      assert course.slug == "some slug"
    end

    test "create_course/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Courses.create_course(@invalid_attrs)
    end

    test "update_course/2 with valid data updates the course" do
      course = course_fixture()

      update_attrs = %{
        description: "some updated description",
        title: "some updated title",
        subtitle: "some updated subtitle",
        start_date: ~D[2024-04-03],
        end_date: ~D[2024-04-03],
        tuition: "456.7",
        deposit: "456.7",
        slug: "some updated slug"
      }

      assert {:ok, %Course{} = course} = Courses.update_course(course, update_attrs)
      assert course.description == "some updated description"
      assert course.title == "some updated title"
      assert course.subtitle == "some updated subtitle"
      assert course.start_date == ~D[2024-04-03]
      assert course.end_date == ~D[2024-04-03]
      assert course.tuition == Money.new(@default_currency, "456.7")
      assert course.deposit == Money.new(@default_currency, "456.7")
      assert course.slug == "some updated slug"
    end

    test "update_course/2 with invalid data returns error changeset" do
      course = course_fixture()
      assert {:error, %Ecto.Changeset{}} = Courses.update_course(course, @invalid_attrs)
      assert course == Courses.get_course!(course.id)
    end

    test "delete_course/1 deletes the course" do
      course = course_fixture()
      assert {:ok, %Course{}} = Courses.delete_course(course)
      assert_raise Ecto.NoResultsError, fn -> Courses.get_course!(course.id) end
    end

    test "change_course/1 returns a course changeset" do
      course = course_fixture()
      assert %Ecto.Changeset{} = Courses.change_course(course)
    end
  end

  describe "course_sessions" do
    alias StudyGarden.Courses.CourseSession

    import StudyGarden.CoursesFixtures

    @invalid_attrs %{start: nil, end: nil, zoom_url: nil}

    # TODO: fix association loading
    # test "list_course_sessions/0 returns all course_sessions" do
    #   course_session = course_session_fixture()
    #   assert Courses.list_course_sessions() == [course_session]
    # end

    test "get_course_session!/1 returns the course_session with given id" do
      course_session = course_session_fixture()
      assert Courses.get_course_session!(course_session.id) == course_session
    end

    test "create_course_session/1 with valid data creates a course_session" do
      valid_attrs = %{start: ~U[2024-04-21 08:35:00Z], end: ~U[2024-04-21 08:35:00Z], zoom_url: "some zoom_url"}

      assert {:ok, %CourseSession{} = course_session} = Courses.create_course_session(valid_attrs)
      assert course_session.start == ~U[2024-04-21 08:35:00Z]
      assert course_session.end == ~U[2024-04-21 08:35:00Z]
      assert course_session.zoom_url == "some zoom_url"
    end

    test "create_course_session/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Courses.create_course_session(@invalid_attrs)
    end

    test "update_course_session/2 with valid data updates the course_session" do
      course_session = course_session_fixture()
      update_attrs = %{start: ~U[2024-04-22 08:35:00Z], end: ~U[2024-04-22 08:35:00Z], zoom_url: "some updated zoom_url"}

      assert {:ok, %CourseSession{} = course_session} = Courses.update_course_session(course_session, update_attrs)
      assert course_session.start == ~U[2024-04-22 08:35:00Z]
      assert course_session.end == ~U[2024-04-22 08:35:00Z]
      assert course_session.zoom_url == "some updated zoom_url"
    end

    test "update_course_session/2 with invalid data returns error changeset" do
      course_session = course_session_fixture()
      assert {:error, %Ecto.Changeset{}} = Courses.update_course_session(course_session, @invalid_attrs)
      assert course_session == Courses.get_course_session!(course_session.id)
    end

    test "delete_course_session/1 deletes the course_session" do
      course_session = course_session_fixture()
      assert {:ok, %CourseSession{}} = Courses.delete_course_session(course_session)
      assert_raise Ecto.NoResultsError, fn -> Courses.get_course_session!(course_session.id) end
    end

    test "change_course_session/1 returns a course_session changeset" do
      course_session = course_session_fixture()
      assert %Ecto.Changeset{} = Courses.change_course_session(course_session)
    end
  end

  describe "course_sign_ups" do
    alias StudyGarden.Courses.CourseSignUp

    import StudyGarden.CoursesFixtures

    @invalid_attrs %{name: nil, email: nil, discord_username: nil, payment_option: nil}

    test "list_course_sign_ups/0 returns all course_sign_ups" do
      course_sign_up = course_sign_up_fixture()
      assert Courses.list_course_sign_ups() == [course_sign_up]
    end

    test "get_course_sign_up!/1 returns the course_sign_up with given id" do
      course_sign_up = course_sign_up_fixture()
      assert Courses.get_course_sign_up!(course_sign_up.id) == course_sign_up
    end

    test "create_course_sign_up/1 with valid data creates a course_sign_up" do
      valid_attrs = %{name: "some name", email: "some email", discord_username: "some discord_username", payment_option: :full}

      assert {:ok, %CourseSignUp{} = course_sign_up} = Courses.create_course_sign_up(valid_attrs)
      assert course_sign_up.name == "some name"
      assert course_sign_up.email == "some email"
      assert course_sign_up.discord_username == "some discord_username"
      assert course_sign_up.payment_option == :full
    end

    test "create_course_sign_up/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Courses.create_course_sign_up(@invalid_attrs)
    end

    test "update_course_sign_up/2 with valid data updates the course_sign_up" do
      course_sign_up = course_sign_up_fixture()
      update_attrs = %{name: "some updated name", email: "some updated email", discord_username: "some updated discord_username", payment_option: :deposit}

      assert {:ok, %CourseSignUp{} = course_sign_up} = Courses.update_course_sign_up(course_sign_up, update_attrs)
      assert course_sign_up.name == "some updated name"
      assert course_sign_up.email == "some updated email"
      assert course_sign_up.discord_username == "some updated discord_username"
      assert course_sign_up.payment_option == :deposit
    end

    test "update_course_sign_up/2 with invalid data returns error changeset" do
      course_sign_up = course_sign_up_fixture()
      assert {:error, %Ecto.Changeset{}} = Courses.update_course_sign_up(course_sign_up, @invalid_attrs)
      assert course_sign_up == Courses.get_course_sign_up!(course_sign_up.id)
    end

    test "delete_course_sign_up/1 deletes the course_sign_up" do
      course_sign_up = course_sign_up_fixture()
      assert {:ok, %CourseSignUp{}} = Courses.delete_course_sign_up(course_sign_up)
      assert_raise Ecto.NoResultsError, fn -> Courses.get_course_sign_up!(course_sign_up.id) end
    end

    test "change_course_sign_up/1 returns a course_sign_up changeset" do
      course_sign_up = course_sign_up_fixture()
      assert %Ecto.Changeset{} = Courses.change_course_sign_up(course_sign_up)
    end
  end
end

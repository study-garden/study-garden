defmodule StudyGarden.DiscordTest do
  use StudyGarden.DataCase

  alias StudyGarden.Discord

  describe "discord_guild_roles" do
    alias StudyGarden.Discord.GuildRole

    import StudyGarden.DiscordFixtures

    @invalid_attrs %{
      discord_id: nil,
      discord_name: nil,
      discord_position: nil,
      discord_color: nil,
      discord_managed: nil
    }

    test "list_discord_guild_roles/0 returns all discord_guild_roles" do
      guild_role = guild_role_fixture()
      assert Discord.list_discord_guild_roles() == [guild_role]
    end

    test "get_guild_role!/1 returns the guild_role with given id" do
      guild_role = guild_role_fixture()
      assert Discord.get_guild_role!(guild_role.id) == guild_role
    end

    test "create_guild_role/1 with valid data creates a guild_role" do
      valid_attrs = %{
        discord_id: "some discord_id",
        discord_name: "some discord_name",
        discord_position: 42,
        discord_color: 42,
        discord_managed: true
      }

      assert {:ok, %GuildRole{} = guild_role} = Discord.create_guild_role(valid_attrs)
      assert guild_role.discord_id == "some discord_id"
      assert guild_role.discord_name == "some discord_name"
      assert guild_role.discord_position == 42
      assert guild_role.discord_color == 42
      assert guild_role.discord_managed == true
    end

    test "create_guild_role/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Discord.create_guild_role(@invalid_attrs)
    end

    test "update_guild_role/2 with valid data updates the guild_role" do
      guild_role = guild_role_fixture()

      update_attrs = %{
        discord_id: "some updated discord_id",
        discord_name: "some updated discord_name",
        discord_position: 43,
        discord_color: 43,
        discord_managed: false
      }

      assert {:ok, %GuildRole{} = guild_role} =
               Discord.update_guild_role(guild_role, update_attrs)

      assert guild_role.discord_id == "some updated discord_id"
      assert guild_role.discord_name == "some updated discord_name"
      assert guild_role.discord_position == 43
      assert guild_role.discord_color == 43
      assert guild_role.discord_managed == false
    end

    test "update_guild_role/2 with invalid data returns error changeset" do
      guild_role = guild_role_fixture()
      assert {:error, %Ecto.Changeset{}} = Discord.update_guild_role(guild_role, @invalid_attrs)
      assert guild_role == Discord.get_guild_role!(guild_role.id)
    end

    test "delete_guild_role/1 deletes the guild_role" do
      guild_role = guild_role_fixture()
      assert {:ok, %GuildRole{}} = Discord.delete_guild_role(guild_role)
      assert_raise Ecto.NoResultsError, fn -> Discord.get_guild_role!(guild_role.id) end
    end

    test "change_guild_role/1 returns a guild_role changeset" do
      guild_role = guild_role_fixture()
      assert %Ecto.Changeset{} = Discord.change_guild_role(guild_role)
    end
  end
end

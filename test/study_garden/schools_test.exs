defmodule StudyGarden.SchoolsTest do
  use StudyGarden.DataCase

  alias StudyGarden.Schools

  describe "schools" do
    alias StudyGarden.Schools.School

    import StudyGarden.SchoolsFixtures

    @invalid_attrs %{
      description: nil,
      full_name: nil,
      short_name: nil,
      timezone: nil,
      stripe_customer_portal_url: nil,
      google_calendar_embed_url: nil
    }

    test "update_school/2 with valid data updates the school" do
      school = Schools.get_school!()

      update_attrs = %{
        description: "some updated description",
        full_name: "some updated full_name",
        short_name: "some updated short_name",
        timezone: "some updated timezone",
        stripe_customer_portal_url: "some updated stripe_customer_portal_url",
        google_calendar_embed_url: "some updated google_calendar_embed_url"
      }

      assert {:ok, %School{} = school} =
               Schools.update_school(school, update_attrs)

      assert school.description == "some updated description"
      assert school.full_name == "some updated full_name"
      assert school.short_name == "some updated short_name"
      assert school.timezone == "some updated timezone"
      assert school.stripe_customer_portal_url == "some updated stripe_customer_portal_url"
      assert school.google_calendar_embed_url == "some updated google_calendar_embed_url"
    end

    test "update_school/2 with invalid data returns error changeset" do
      school = Schools.get_school!()
      assert {:error, %Ecto.Changeset{}} = Schools.update_school(school, @invalid_attrs)
      assert school == Schools.get_school!()
    end

    test "change_school/1 returns a school changeset" do
      school = Schools.get_school!()
      assert %Ecto.Changeset{} = Schools.change_school(school)
    end
  end

  describe "school_announcements" do
    alias StudyGarden.Schools.Announcement

    import StudyGarden.SchoolsFixtures

    @invalid_attrs %{message: nil, pinned: nil}

    test "list_school_announcements/0 returns all school_announcements" do
      announcement = announcement_fixture()
      assert Schools.list_school_announcements() == [announcement]
    end

    test "get_announcement!/1 returns the announcement with given id" do
      announcement = announcement_fixture()
      assert Schools.get_announcement!(announcement.id) == announcement
    end

    test "create_announcement/1 with valid data creates a announcement" do
      valid_attrs = %{message: "some message", pinned: true}

      assert {:ok, %Announcement{} = announcement} = Schools.create_announcement(valid_attrs)
      assert announcement.message == "some message"
      assert announcement.pinned == true
    end

    test "create_announcement/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Schools.create_announcement(@invalid_attrs)
    end

    test "update_announcement/2 with valid data updates the announcement" do
      announcement = announcement_fixture()
      update_attrs = %{message: "some updated message", pinned: false}

      assert {:ok, %Announcement{} = announcement} =
               Schools.update_announcement(announcement, update_attrs)

      assert announcement.message == "some updated message"
      assert announcement.pinned == false
    end

    test "update_announcement/2 with invalid data returns error changeset" do
      announcement = announcement_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Schools.update_announcement(announcement, @invalid_attrs)

      assert announcement == Schools.get_announcement!(announcement.id)
    end

    test "delete_announcement/1 deletes the announcement" do
      announcement = announcement_fixture()
      assert {:ok, %Announcement{}} = Schools.delete_announcement(announcement)
      assert_raise Ecto.NoResultsError, fn -> Schools.get_announcement!(announcement.id) end
    end

    test "change_announcement/1 returns a announcement changeset" do
      announcement = announcement_fixture()
      assert %Ecto.Changeset{} = Schools.change_announcement(announcement)
    end
  end
end

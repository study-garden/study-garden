defmodule StudyGarden.StripeTest do
  use StudyGarden.DataCase

  alias StudyGarden.Stripe

  describe "stripe_webhook_events" do
    alias StudyGarden.Stripe.StripeWebhookEvent

    import StudyGarden.StripeFixtures

    @invalid_attrs %{host: nil, stripe_id: nil, stripe_type: nil, stripe_created: nil, stripe_api_version: nil, stripe_idempotency_key: nil, stripe_signature: nil}

    test "list_stripe_webhook_events/0 returns all stripe_webhook_events" do
      stripe_webhook_event = stripe_webhook_event_fixture()
      assert Stripe.list_stripe_webhook_events() == [stripe_webhook_event]
    end

    test "get_stripe_webhook_event!/1 returns the stripe_webhook_event with given id" do
      stripe_webhook_event = stripe_webhook_event_fixture()
      assert Stripe.get_stripe_webhook_event!(stripe_webhook_event.id) == stripe_webhook_event
    end

    test "create_stripe_webhook_event/1 with valid data creates a stripe_webhook_event" do
      valid_attrs = %{host: "some host", stripe_id: "some stripe_id", stripe_request_id: "req_somestriperequestid", stripe_type: "some stripe_type", stripe_created: 42, stripe_api_version: "some stripe_api_version", stripe_idempotency_key: "some stripe_idempotency_key", stripe_signature: "some stripe_signature", }

      assert {:ok, %StripeWebhookEvent{} = stripe_webhook_event} = Stripe.create_stripe_webhook_event(valid_attrs)
      assert stripe_webhook_event.host == "some host"
      assert stripe_webhook_event.stripe_id == "some stripe_id"
      assert stripe_webhook_event.stripe_request_id == "req_somestriperequestid"
      assert stripe_webhook_event.stripe_type == "some stripe_type"
      assert stripe_webhook_event.stripe_created == 42
      assert stripe_webhook_event.stripe_api_version == "some stripe_api_version"
      assert stripe_webhook_event.stripe_idempotency_key == "some stripe_idempotency_key"
      assert stripe_webhook_event.stripe_signature == "some stripe_signature"
    end

    test "create_stripe_webhook_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stripe.create_stripe_webhook_event(@invalid_attrs)
    end

    test "update_stripe_webhook_event/2 with valid data updates the stripe_webhook_event" do
      stripe_webhook_event = stripe_webhook_event_fixture()
      update_attrs = %{host: "some updated host", stripe_id: "some updated stripe_id", stripe_type: "some updated stripe_type", stripe_created: 43, stripe_api_version: "some updated stripe_api_version", stripe_idempotency_key: "some updated stripe_idempotency_key", stripe_signature: "some updated stripe_signature"}

      assert {:ok, %StripeWebhookEvent{} = stripe_webhook_event} = Stripe.update_stripe_webhook_event(stripe_webhook_event, update_attrs)
      assert stripe_webhook_event.host == "some updated host"
      assert stripe_webhook_event.stripe_id == "some updated stripe_id"
      assert stripe_webhook_event.stripe_type == "some updated stripe_type"
      assert stripe_webhook_event.stripe_created == 43
      assert stripe_webhook_event.stripe_api_version == "some updated stripe_api_version"
      assert stripe_webhook_event.stripe_idempotency_key == "some updated stripe_idempotency_key"
      assert stripe_webhook_event.stripe_signature == "some updated stripe_signature"
    end

    test "update_stripe_webhook_event/2 with invalid data returns error changeset" do
      stripe_webhook_event = stripe_webhook_event_fixture()
      assert {:error, %Ecto.Changeset{}} = Stripe.update_stripe_webhook_event(stripe_webhook_event, @invalid_attrs)
      assert stripe_webhook_event == Stripe.get_stripe_webhook_event!(stripe_webhook_event.id)
    end

    test "delete_stripe_webhook_event/1 deletes the stripe_webhook_event" do
      stripe_webhook_event = stripe_webhook_event_fixture()
      assert {:ok, %StripeWebhookEvent{}} = Stripe.delete_stripe_webhook_event(stripe_webhook_event)
      assert_raise Ecto.NoResultsError, fn -> Stripe.get_stripe_webhook_event!(stripe_webhook_event.id) end
    end

    test "change_stripe_webhook_event/1 returns a stripe_webhook_event changeset" do
      stripe_webhook_event = stripe_webhook_event_fixture()
      assert %Ecto.Changeset{} = Stripe.change_stripe_webhook_event(stripe_webhook_event)
    end
  end

  describe "stripe_products" do
    alias StudyGarden.Stripe.StripeProduct

    import StudyGarden.StripeFixtures

    @invalid_attrs %{stripe_id: nil, stripe_name: nil, stripe_active: nil, default_price: nil, stripe_metadata: nil, stripe_created: nil, stripe_updated: nil, stripe_url: nil}

    test "list_stripe_products/0 returns all stripe_products" do
      stripe_product = stripe_product_fixture()
      assert Stripe.list_stripe_products() == [stripe_product]
    end

    test "get_stripe_product!/1 returns the stripe_product with given id" do
      stripe_product = stripe_product_fixture()
      assert Stripe.get_stripe_product!(stripe_product.id) == stripe_product
    end

    test "create_stripe_product/1 with valid data creates a stripe_product" do
      valid_attrs = %{stripe_id: "some stripe_id", stripe_name: "some stripe_name", stripe_active: true, default_price: "some default_price", stripe_metadata: %{}, stripe_created: 42, stripe_updated: 42, stripe_url: "some stripe_url"}

      assert {:ok, %StripeProduct{} = stripe_product} = Stripe.create_stripe_product(valid_attrs)
      assert stripe_product.stripe_id == "some stripe_id"
      assert stripe_product.stripe_name == "some stripe_name"
      assert stripe_product.stripe_active == true
      assert stripe_product.default_price == "some default_price"
      assert stripe_product.stripe_metadata == %{}
      assert stripe_product.stripe_created == 42
      assert stripe_product.stripe_updated == 42
      assert stripe_product.stripe_url == "some stripe_url"
    end

    test "create_stripe_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stripe.create_stripe_product(@invalid_attrs)
    end

    test "update_stripe_product/2 with valid data updates the stripe_product" do
      stripe_product = stripe_product_fixture()
      update_attrs = %{stripe_id: "some updated stripe_id", stripe_name: "some updated stripe_name", stripe_active: false, default_price: "some updated default_price", stripe_metadata: %{}, stripe_created: 43, stripe_updated: 43, stripe_url: "some updated stripe_url"}

      assert {:ok, %StripeProduct{} = stripe_product} = Stripe.update_stripe_product(stripe_product, update_attrs)
      assert stripe_product.stripe_id == "some updated stripe_id"
      assert stripe_product.stripe_name == "some updated stripe_name"
      assert stripe_product.stripe_active == false
      assert stripe_product.default_price == "some updated default_price"
      assert stripe_product.stripe_metadata == %{}
      assert stripe_product.stripe_created == 43
      assert stripe_product.stripe_updated == 43
      assert stripe_product.stripe_url == "some updated stripe_url"
    end

    test "update_stripe_product/2 with invalid data returns error changeset" do
      stripe_product = stripe_product_fixture()
      assert {:error, %Ecto.Changeset{}} = Stripe.update_stripe_product(stripe_product, @invalid_attrs)
      assert stripe_product == Stripe.get_stripe_product!(stripe_product.id)
    end

    test "delete_stripe_product/1 deletes the stripe_product" do
      stripe_product = stripe_product_fixture()
      assert {:ok, %StripeProduct{}} = Stripe.delete_stripe_product(stripe_product)
      assert_raise Ecto.NoResultsError, fn -> Stripe.get_stripe_product!(stripe_product.id) end
    end

    test "change_stripe_product/1 returns a stripe_product changeset" do
      stripe_product = stripe_product_fixture()
      assert %Ecto.Changeset{} = Stripe.change_stripe_product(stripe_product)
    end
  end

  describe "stripe_customers" do
    alias StudyGarden.Stripe.StripeCustomer

    import StudyGarden.StripeFixtures

    @invalid_attrs %{stripe_id: nil}

    test "list_stripe_customers/0 returns all stripe_customers" do
      stripe_customer = stripe_customer_fixture()
      assert Stripe.list_stripe_customers() == [stripe_customer]
    end

    test "get_stripe_customer!/1 returns the stripe_customer with given id" do
      stripe_customer = stripe_customer_fixture()
      assert Stripe.get_stripe_customer!(stripe_customer.id) == stripe_customer
    end

    test "create_stripe_customer/1 with valid data creates a stripe_customer" do
      valid_attrs = %{stripe_id: "some stripe_id"}

      assert {:ok, %StripeCustomer{} = stripe_customer} = Stripe.create_stripe_customer(valid_attrs)
      assert stripe_customer.stripe_id == "some stripe_id"
    end

    test "create_stripe_customer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stripe.create_stripe_customer(@invalid_attrs)
    end

    test "update_stripe_customer/2 with valid data updates the stripe_customer" do
      stripe_customer = stripe_customer_fixture()
      update_attrs = %{stripe_id: "some updated stripe_id"}

      assert {:ok, %StripeCustomer{} = stripe_customer} = Stripe.update_stripe_customer(stripe_customer, update_attrs)
      assert stripe_customer.stripe_id == "some updated stripe_id"
    end

    test "update_stripe_customer/2 with invalid data returns error changeset" do
      stripe_customer = stripe_customer_fixture()
      assert {:error, %Ecto.Changeset{}} = Stripe.update_stripe_customer(stripe_customer, @invalid_attrs)
      assert stripe_customer == Stripe.get_stripe_customer!(stripe_customer.id)
    end

    test "delete_stripe_customer/1 deletes the stripe_customer" do
      stripe_customer = stripe_customer_fixture()
      assert {:ok, %StripeCustomer{}} = Stripe.delete_stripe_customer(stripe_customer)
      assert_raise Ecto.NoResultsError, fn -> Stripe.get_stripe_customer!(stripe_customer.id) end
    end

    test "change_stripe_customer/1 returns a stripe_customer changeset" do
      stripe_customer = stripe_customer_fixture()
      assert %Ecto.Changeset{} = Stripe.change_stripe_customer(stripe_customer)
    end
  end

  describe "stripe_subscriptions" do
    alias StudyGarden.Stripe.StripeSubscription

    import StudyGarden.StripeFixtures

    @invalid_attrs %{stripe_id: nil, stripe_cancel_at_period_end: nil, stripe_current_period_start: nil, stripe_current_period_end: nil, stripe_ended_at: nil, stripe_customer: nil, stripe_items: nil, stripe_latest_invoice: nil, stripe_metadata: nil, stripe_status: nil, stripe_cancel_at: nil, stripe_canceled_at: nil, stripe_created: nil, stripe_days_until_due: nil, stripe_start_date: nil, sg_stripe_key: nil}

    test "list_stripe_subscriptions/0 returns all stripe_subscriptions" do
      stripe_subscription = stripe_subscription_fixture()
      assert Stripe.list_stripe_subscriptions() == [stripe_subscription]
    end

    test "get_stripe_subscription!/1 returns the stripe_subscription with given id" do
      stripe_subscription = stripe_subscription_fixture()
      assert Stripe.get_stripe_subscription!(stripe_subscription.id) == stripe_subscription
    end

    test "create_stripe_subscription/1 with valid data creates a stripe_subscription" do
      valid_attrs = %{stripe_id: "some stripe_id", stripe_cancel_at_period_end: true, stripe_current_period_start: 42, stripe_current_period_end: 42, stripe_ended_at: 42, stripe_customer: "some stripe_customer", stripe_items: %{}, stripe_latest_invoice: "some stripe_latest_invoice", stripe_metadata: %{}, stripe_status: :incomplete, stripe_cancel_at: 42, stripe_canceled_at: 42, stripe_created: 42, stripe_days_until_due: 42, stripe_start_date: 42, sg_stripe_key: "some sg_stripe_key"}

      assert {:ok, %StripeSubscription{} = stripe_subscription} = Stripe.create_stripe_subscription(valid_attrs)
      assert stripe_subscription.stripe_id == "some stripe_id"
      assert stripe_subscription.stripe_cancel_at_period_end == true
      assert stripe_subscription.stripe_current_period_start == 42
      assert stripe_subscription.stripe_current_period_end == 42
      assert stripe_subscription.stripe_ended_at == 42
      assert stripe_subscription.stripe_customer == "some stripe_customer"
      assert stripe_subscription.stripe_items == %{}
      assert stripe_subscription.stripe_latest_invoice == "some stripe_latest_invoice"
      assert stripe_subscription.stripe_metadata == %{}
      assert stripe_subscription.stripe_status == :incomplete
      assert stripe_subscription.stripe_cancel_at == 42
      assert stripe_subscription.stripe_canceled_at == 42
      assert stripe_subscription.stripe_created == 42
      assert stripe_subscription.stripe_days_until_due == 42
      assert stripe_subscription.stripe_start_date == 42
      assert stripe_subscription.sg_stripe_key == "some sg_stripe_key"
    end

    test "create_stripe_subscription/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stripe.create_stripe_subscription(@invalid_attrs)
    end

    test "update_stripe_subscription/2 with valid data updates the stripe_subscription" do
      stripe_subscription = stripe_subscription_fixture()
      update_attrs = %{stripe_id: "some updated stripe_id", stripe_cancel_at_period_end: false, stripe_current_period_start: 43, stripe_current_period_end: 43, stripe_ended_at: 43, stripe_customer: "some updated stripe_customer", stripe_items: %{}, stripe_latest_invoice: "some updated stripe_latest_invoice", stripe_metadata: %{}, stripe_status: :incomplete_expired, stripe_cancel_at: 43, stripe_canceled_at: 43, stripe_created: 43, stripe_days_until_due: 43, stripe_start_date: 43, sg_stripe_key: "some updated sg_stripe_key"}

      assert {:ok, %StripeSubscription{} = stripe_subscription} = Stripe.update_stripe_subscription(stripe_subscription, update_attrs)
      assert stripe_subscription.stripe_id == "some updated stripe_id"
      assert stripe_subscription.stripe_cancel_at_period_end == false
      assert stripe_subscription.stripe_current_period_start == 43
      assert stripe_subscription.stripe_current_period_end == 43
      assert stripe_subscription.stripe_ended_at == 43
      assert stripe_subscription.stripe_customer == "some updated stripe_customer"
      assert stripe_subscription.stripe_items == %{}
      assert stripe_subscription.stripe_latest_invoice == "some updated stripe_latest_invoice"
      assert stripe_subscription.stripe_metadata == %{}
      assert stripe_subscription.stripe_status == :incomplete_expired
      assert stripe_subscription.stripe_cancel_at == 43
      assert stripe_subscription.stripe_canceled_at == 43
      assert stripe_subscription.stripe_created == 43
      assert stripe_subscription.stripe_days_until_due == 43
      assert stripe_subscription.stripe_start_date == 43
      assert stripe_subscription.sg_stripe_key == "some updated sg_stripe_key"
    end

    test "update_stripe_subscription/2 with invalid data returns error changeset" do
      stripe_subscription = stripe_subscription_fixture()
      assert {:error, %Ecto.Changeset{}} = Stripe.update_stripe_subscription(stripe_subscription, @invalid_attrs)
      assert stripe_subscription == Stripe.get_stripe_subscription!(stripe_subscription.id)
    end

    test "delete_stripe_subscription/1 deletes the stripe_subscription" do
      stripe_subscription = stripe_subscription_fixture()
      assert {:ok, %StripeSubscription{}} = Stripe.delete_stripe_subscription(stripe_subscription)
      assert_raise Ecto.NoResultsError, fn -> Stripe.get_stripe_subscription!(stripe_subscription.id) end
    end

    test "change_stripe_subscription/1 returns a stripe_subscription changeset" do
      stripe_subscription = stripe_subscription_fixture()
      assert %Ecto.Changeset{} = Stripe.change_stripe_subscription(stripe_subscription)
    end
  end
end

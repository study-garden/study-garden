defmodule StudyGarden.AccessTest do
  use StudyGarden.DataCase

  alias StudyGarden.Access

  describe "roles" do
    alias StudyGarden.Access.Role

    import StudyGarden.AccessFixtures

    @invalid_attrs %{name: nil}

    test "list_roles/0 returns all roles" do
      role = role_fixture()
      assert Access.list_roles() == [role]
    end

    test "get_role!/1 returns the role with given id" do
      role = role_fixture()
      assert Access.get_role!(role.id) == role
    end

    test "create_role/1 with valid data creates a role" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Role{} = role} = Access.create_role(valid_attrs)
      assert role.name == "some name"
    end

    test "create_role/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Access.create_role(@invalid_attrs)
    end

    test "update_role/2 with valid data updates the role" do
      role = role_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Role{} = role} = Access.update_role(role, update_attrs)
      assert role.name == "some updated name"
    end

    test "update_role/2 with invalid data returns error changeset" do
      role = role_fixture()
      assert {:error, %Ecto.Changeset{}} = Access.update_role(role, @invalid_attrs)
      assert role == Access.get_role!(role.id)
    end

    test "delete_role/1 deletes the role" do
      role = role_fixture()
      assert {:ok, %Role{}} = Access.delete_role(role)
      assert_raise Ecto.NoResultsError, fn -> Access.get_role!(role.id) end
    end

    test "change_role/1 returns a role changeset" do
      role = role_fixture()
      assert %Ecto.Changeset{} = Access.change_role(role)
    end
  end

  describe "user_roles" do
    alias StudyGarden.Access.UserRole

    import StudyGarden.IdentitiesFixtures
    import StudyGarden.AccessFixtures

    @invalid_attrs %{}

    test "list_user_roles/0 returns all user_roles" do
      user_role = user_role_fixture()
      assert Access.list_user_roles() == [user_role]
    end

    test "get_user_role!/1 returns the user_role with given id" do
      user_role = user_role_fixture()
      assert Access.get_user_role!(user_role.id) == user_role
    end

    test "create_user_role/1 with valid data creates a user_role" do
      user = user_fixture()
      role = role_fixture()

      valid_attrs = %{
        user_id: user.id,
        role_id: role.id
      }

      assert {:ok, %UserRole{} = user_role} = Access.create_user_role(valid_attrs)
      assert user_role.user_id == user.id
      assert user_role.role_id == role.id
    end

    test "create_user_role/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Access.create_user_role(@invalid_attrs)
    end

    test "delete_user_role/1 deletes the user_role" do
      user_role = user_role_fixture()
      assert {:ok, %UserRole{}} = Access.delete_user_role(user_role)
      assert_raise Ecto.NoResultsError, fn -> Access.get_user_role!(user_role.id) end
    end

    test "change_user_role/1 returns a user_role changeset" do
      user_role = user_role_fixture()
      assert %Ecto.Changeset{} = Access.change_user_role(user_role)
    end
  end
end

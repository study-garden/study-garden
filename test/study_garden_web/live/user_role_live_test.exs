defmodule StudyGardenWeb.UserRoleLiveTest do
  use StudyGardenWeb.ConnCase

  import Phoenix.LiveViewTest
  import StudyGarden.IdentitiesFixtures
  import StudyGarden.AccessFixtures

  defp create_user(_) do
    user = user_fixture()
    %{user: user}
  end

  defp create_role(_) do
    role = role_fixture()
    %{role: role}
  end

  defp create_user_role(_) do
    user_role = user_role_fixture()
    %{user_role: user_role}
  end

  describe "Index" do
    setup [:create_user, :create_role, :create_user_role]

    setup %{conn: conn} do
      user = user_fixture()
      %{conn: log_in_user(conn, user), user: user}
    end

    test "lists all user_roles", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/user_roles")

      assert html =~ "Listing User roles"
    end

    test "saves new user_role", %{conn: conn, user: user, role: role} do
      user = user_fixture()
      create_attrs = %{user_id: user.id, role_id: role.id}

      {:ok, index_live, _html} = live(conn, ~p"/user_roles")

      assert index_live |> element("a", "New User role") |> render_click() =~
               "New User role"

      assert_patch(index_live, ~p"/user_roles/new")

      assert index_live
             |> form("#user_role-form", user_role: create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/user_roles")

      html = render(index_live)
      assert html =~ "User role created successfully"
    end

    test "deletes user_role in listing", %{conn: conn, user_role: user_role} do
      {:ok, index_live, _html} = live(conn, ~p"/user_roles")

      assert index_live |> element("#user_roles-#{user_role.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#user_roles-#{user_role.id}")
    end
  end

  describe "Show" do
    setup [:create_user_role]

    setup %{conn: conn} do
      user = user_fixture()
      %{conn: log_in_user(conn, user), user: user}
    end

    test "displays user_role", %{conn: conn, user_role: user_role} do
      {:ok, _show_live, html} = live(conn, ~p"/user_roles/#{user_role}")

      assert html =~ "Show User role"
    end
  end
end

defmodule StudyGardenWeb.StripeSubscriptionLiveTest do
  use StudyGardenWeb.ConnCase

  import Phoenix.LiveViewTest
  import StudyGarden.StripeFixtures

  @create_attrs %{stripe_id: "some stripe_id", stripe_cancel_at_period_end: true, stripe_current_period_start: 42, stripe_current_period_end: 42, stripe_ended_at: 42, stripe_customer: "some stripe_customer", stripe_items: %{}, stripe_latest_invoice: "some stripe_latest_invoice", stripe_metadata: %{}, stripe_status: :incomplete, stripe_cancel_at: 42, stripe_canceled_at: 42, stripe_created: 42, stripe_days_until_due: 42, stripe_start_date: 42, sg_stripe_key: "some sg_stripe_key"}
  @update_attrs %{stripe_id: "some updated stripe_id", stripe_cancel_at_period_end: false, stripe_current_period_start: 43, stripe_current_period_end: 43, stripe_ended_at: 43, stripe_customer: "some updated stripe_customer", stripe_items: %{}, stripe_latest_invoice: "some updated stripe_latest_invoice", stripe_metadata: %{}, stripe_status: :incomplete_expired, stripe_cancel_at: 43, stripe_canceled_at: 43, stripe_created: 43, stripe_days_until_due: 43, stripe_start_date: 43, sg_stripe_key: "some updated sg_stripe_key"}
  @invalid_attrs %{stripe_id: nil, stripe_cancel_at_period_end: false, stripe_current_period_start: nil, stripe_current_period_end: nil, stripe_ended_at: nil, stripe_customer: nil, stripe_items: nil, stripe_latest_invoice: nil, stripe_metadata: nil, stripe_status: nil, stripe_cancel_at: nil, stripe_canceled_at: nil, stripe_created: nil, stripe_days_until_due: nil, stripe_start_date: nil, sg_stripe_key: nil}

  defp create_stripe_subscription(_) do
    stripe_subscription = stripe_subscription_fixture()
    %{stripe_subscription: stripe_subscription}
  end

  describe "Index" do
    setup [:create_stripe_subscription]

    test "lists all stripe_subscriptions", %{conn: conn, stripe_subscription: stripe_subscription} do
      {:ok, _index_live, html} = live(conn, ~p"/stripe_subscriptions")

      assert html =~ "Listing Stripe subscriptions"
      assert html =~ stripe_subscription.stripe_id
    end

    test "saves new stripe_subscription", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/stripe_subscriptions")

      assert index_live |> element("a", "New Stripe subscription") |> render_click() =~
               "New Stripe subscription"

      assert_patch(index_live, ~p"/stripe_subscriptions/new")

      assert index_live
             |> form("#stripe_subscription-form", stripe_subscription: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#stripe_subscription-form", stripe_subscription: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/stripe_subscriptions")

      html = render(index_live)
      assert html =~ "Stripe subscription created successfully"
      assert html =~ "some stripe_id"
    end

    test "updates stripe_subscription in listing", %{conn: conn, stripe_subscription: stripe_subscription} do
      {:ok, index_live, _html} = live(conn, ~p"/stripe_subscriptions")

      assert index_live |> element("#stripe_subscriptions-#{stripe_subscription.id} a", "Edit") |> render_click() =~
               "Edit Stripe subscription"

      assert_patch(index_live, ~p"/stripe_subscriptions/#{stripe_subscription}/edit")

      assert index_live
             |> form("#stripe_subscription-form", stripe_subscription: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#stripe_subscription-form", stripe_subscription: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/stripe_subscriptions")

      html = render(index_live)
      assert html =~ "Stripe subscription updated successfully"
      assert html =~ "some updated stripe_id"
    end

    test "deletes stripe_subscription in listing", %{conn: conn, stripe_subscription: stripe_subscription} do
      {:ok, index_live, _html} = live(conn, ~p"/stripe_subscriptions")

      assert index_live |> element("#stripe_subscriptions-#{stripe_subscription.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#stripe_subscriptions-#{stripe_subscription.id}")
    end
  end

  describe "Show" do
    setup [:create_stripe_subscription]

    test "displays stripe_subscription", %{conn: conn, stripe_subscription: stripe_subscription} do
      {:ok, _show_live, html} = live(conn, ~p"/stripe_subscriptions/#{stripe_subscription}")

      assert html =~ "Show Stripe subscription"
      assert html =~ stripe_subscription.stripe_id
    end

    test "updates stripe_subscription within modal", %{conn: conn, stripe_subscription: stripe_subscription} do
      {:ok, show_live, _html} = live(conn, ~p"/stripe_subscriptions/#{stripe_subscription}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Stripe subscription"

      assert_patch(show_live, ~p"/stripe_subscriptions/#{stripe_subscription}/show/edit")

      assert show_live
             |> form("#stripe_subscription-form", stripe_subscription: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#stripe_subscription-form", stripe_subscription: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/stripe_subscriptions/#{stripe_subscription}")

      html = render(show_live)
      assert html =~ "Stripe subscription updated successfully"
      assert html =~ "some updated stripe_id"
    end
  end
end

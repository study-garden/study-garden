defmodule StudyGardenWeb.SchoolLiveTest do
  use StudyGardenWeb.ConnCase

  import Phoenix.LiveViewTest
  import StudyGarden.SchoolsFixtures

  @create_attrs %{
    description: "some description",
    full_name: "some full_name",
    short_name: "some short_name",
    timezone: "some timezone",
    stripe_customer_portal_url: "some stripe_customer_portal_url",
    google_calendar_embed_url: "some google_calendar_embed_url"
  }
  @update_attrs %{
    description: "some updated description",
    full_name: "some updated full_name",
    short_name: "some updated short_name",
    timezone: "some updated timezone",
    stripe_customer_portal_url: "some updated stripe_customer_portal_url",
    google_calendar_embed_url: "some updated google_calendar_embed_url"
  }
  @invalid_attrs %{
    description: nil,
    full_name: nil,
    short_name: nil,
    timezone: nil,
    stripe_customer_portal_url: nil,
    google_calendar_embed_url: nil
  }

  defp create_school(_) do
    school = school_fixture()
    %{school: school}
  end

  describe "About" do
    setup [:create_school]

    # test "displays school", %{conn: conn, school: school} do
    #   {:ok, _show_live, html} = live(conn, ~p"/about")

    #   assert html =~ "Show School"
    #   assert html =~ school.description
    # end

    # test "updates school within modal", %{conn: conn, school: school} do
    #   {:ok, show_live, _html} = live(conn, ~p"/about")

    #   assert show_live |> element("a", "Edit") |> render_click() =~
    #            "Edit School"

    #   assert_patch(show_live, ~p"/school/edit")

    #   assert show_live
    #          |> form("#school-form", school: @invalid_attrs)
    #          |> render_change() =~ "can&#39;t be blank"

    #   assert show_live
    #          |> form("#school-form", school: @update_attrs)
    #          |> render_submit()

    #   assert_patch(show_live, ~p"/school/edit")

    #   html = render(show_live)
    #   assert html =~ "School updated successfully"
    #   assert html =~ "some updated description"
    # end
  end
end

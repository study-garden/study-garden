# defmodule StudyGardenWeb.CourseSessionLiveTest do
#   use StudyGardenWeb.ConnCase

#   import Phoenix.LiveViewTest
#   import StudyGarden.CoursesFixtures

#   @create_attrs %{start: "2024-04-21T08:35:00Z", end: "2024-04-21T08:35:00Z", zoom_url: "some zoom_url"}
#   @update_attrs %{start: "2024-04-22T08:35:00Z", end: "2024-04-22T08:35:00Z", zoom_url: "some updated zoom_url"}
#   @invalid_attrs %{start: nil, end: nil, zoom_url: nil}

#   defp create_course_session(_) do
#     course_session = course_session_fixture()
#     %{course_session: course_session}
#   end

#   describe "Index" do
#     setup [:create_course_session]

#     test "lists all course_sessions", %{conn: conn, course_session: course_session} do
#       {:ok, _index_live, html} = live(conn, ~p"/course_sessions")

#       assert html =~ "Listing Course sessions"
#       assert html =~ course_session.zoom_url
#     end

#     test "saves new course_session", %{conn: conn} do
#       {:ok, index_live, _html} = live(conn, ~p"/course_sessions")

#       assert index_live |> element("a", "New Course session") |> render_click() =~
#                "New Course session"

#       assert_patch(index_live, ~p"/course_sessions/new")

#       assert index_live
#              |> form("#course_session-form", course_session: @invalid_attrs)
#              |> render_change() =~ "can&#39;t be blank"

#       assert index_live
#              |> form("#course_session-form", course_session: @create_attrs)
#              |> render_submit()

#       assert_patch(index_live, ~p"/course_sessions")

#       html = render(index_live)
#       assert html =~ "Course session created successfully"
#       assert html =~ "some zoom_url"
#     end

#     test "updates course_session in listing", %{conn: conn, course_session: course_session} do
#       {:ok, index_live, _html} = live(conn, ~p"/course_sessions")

#       assert index_live |> element("#course_sessions-#{course_session.id} a", "Edit") |> render_click() =~
#                "Edit Course session"

#       assert_patch(index_live, ~p"/course_sessions/#{course_session}/edit")

#       assert index_live
#              |> form("#course_session-form", course_session: @invalid_attrs)
#              |> render_change() =~ "can&#39;t be blank"

#       assert index_live
#              |> form("#course_session-form", course_session: @update_attrs)
#              |> render_submit()

#       assert_patch(index_live, ~p"/course_sessions")

#       html = render(index_live)
#       assert html =~ "Course session updated successfully"
#       assert html =~ "some updated zoom_url"
#     end

#     test "deletes course_session in listing", %{conn: conn, course_session: course_session} do
#       {:ok, index_live, _html} = live(conn, ~p"/course_sessions")

#       assert index_live |> element("#course_sessions-#{course_session.id} a", "Delete") |> render_click()
#       refute has_element?(index_live, "#course_sessions-#{course_session.id}")
#     end
#   end

#   describe "Show" do
#     setup [:create_course_session]

#     test "displays course_session", %{conn: conn, course_session: course_session} do
#       {:ok, _show_live, html} = live(conn, ~p"/course_sessions/#{course_session}")

#       assert html =~ "Show Course session"
#       assert html =~ course_session.zoom_url
#     end

#     test "updates course_session within modal", %{conn: conn, course_session: course_session} do
#       {:ok, show_live, _html} = live(conn, ~p"/course_sessions/#{course_session}")

#       assert show_live |> element("a", "Edit") |> render_click() =~
#                "Edit Course session"

#       assert_patch(show_live, ~p"/course_sessions/#{course_session}/show/edit")

#       assert show_live
#              |> form("#course_session-form", course_session: @invalid_attrs)
#              |> render_change() =~ "can&#39;t be blank"

#       assert show_live
#              |> form("#course_session-form", course_session: @update_attrs)
#              |> render_submit()

#       assert_patch(show_live, ~p"/course_sessions/#{course_session}")

#       html = render(show_live)
#       assert html =~ "Course session updated successfully"
#       assert html =~ "some updated zoom_url"
#     end
#   end
# end

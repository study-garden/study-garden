defmodule StudyGardenWeb.CourseLiveTest do
  use StudyGardenWeb.ConnCase

  import Phoenix.LiveViewTest
  import StudyGarden.CoursesFixtures

  @create_attrs %{
    description: "some description",
    title: "some title",
    subtitle: "some subtitle",
    start_date: "2024-04-02",
    end_date: "2024-04-02",
    tuition: "120.5",
    deposit: "120.5",
    slug: "some slug"
  }
  @update_attrs %{
    description: "some updated description",
    title: "some updated title",
    subtitle: "some updated subtitle",
    start_date: "2024-04-03",
    end_date: "2024-04-03",
    tuition: "456.7",
    deposit: "456.7",
    slug: "some updated slug"
  }
  @invalid_attrs %{
    description: nil,
    title: nil,
    subtitle: nil,
    start_date: nil,
    end_date: nil,
    tuition: nil,
    deposit: nil,
    slug: nil
  }

  defp create_course(_) do
    course = course_fixture()
    %{course: course}
  end

  describe "Index" do
    setup [:create_course]

    test "lists all courses", %{conn: conn, course: course} do
      {:ok, _index_live, html} = live(conn, ~p"/courses")

      assert html =~ "Courses"
      assert html =~ course.description
    end
  end

  describe "Show" do
    setup [:create_course]

    test "displays course", %{conn: conn, course: course} do
      {:ok, _show_live, html} = live(conn, ~p"/courses/#{course}")

      assert html =~ "#{course.title}"
      assert html =~ course.description
    end
  end
end

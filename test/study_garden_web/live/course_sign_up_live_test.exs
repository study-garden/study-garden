defmodule StudyGardenWeb.CourseSignUpLiveTest do
  use StudyGardenWeb.ConnCase

  import Phoenix.LiveViewTest
  import StudyGarden.CoursesFixtures

  @create_attrs %{
    name: "some name",
    email: "some email",
    discord_username: "some discord_username",
    payment_option: :full
  }
  @update_attrs %{
    name: "some updated name",
    email: "some updated email",
    discord_username: "some updated discord_username",
    payment_option: :deposit
  }
  @invalid_attrs %{name: nil, email: nil, discord_username: nil, payment_option: nil}

  defp create_course(_) do
    course = course_fixture()
    %{course: course}
  end

  defp create_course_sign_up(_) do
    course_sign_up = course_sign_up_fixture()
    %{course_sign_up: course_sign_up}
  end

  describe "Index" do
    setup [:create_course, :create_course_sign_up]

    test "lists all course_sign_ups", %{conn: conn, course_sign_up: course_sign_up} do
      {:ok, _index_live, html} = live(conn, ~p"/courses/sign-ups")

      assert html =~ "Listing Course sign ups"
      assert html =~ course_sign_up.name
    end

    test "saves new course_sign_up", %{conn: conn, course: course} do
      {:ok, index_live, _html} = live(conn, ~p"/courses/#{course}/sign-ups")

      assert index_live |> element("a", "New Course sign up") |> render_click() =~
               "New Course sign up"

      assert_patch(index_live, ~p"/courses/sign-up/new")

      assert index_live
             |> form("#course_sign_up-form", course_sign_up: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#course_sign_up-form", course_sign_up: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/courses/sign-ups")

      html = render(index_live)
      assert html =~ "Course sign up created successfully"
      assert html =~ "some name"
    end
  end

  describe "Show" do
    setup [:create_course_sign_up]

    test "displays course_sign_up", %{conn: conn, course_sign_up: course_sign_up} do
      {:ok, _show_live, html} = live(conn, ~p"/course_sign_ups/#{course_sign_up}")

      assert html =~ "Show Course sign up"
      assert html =~ course_sign_up.name
    end

    test "updates course_sign_up within modal", %{conn: conn, course_sign_up: course_sign_up} do
      {:ok, show_live, _html} = live(conn, ~p"/course_sign_ups/#{course_sign_up}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Course sign up"

      assert_patch(show_live, ~p"/course_sign_ups/#{course_sign_up}/show/edit")

      assert show_live
             |> form("#course_sign_up-form", course_sign_up: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#course_sign_up-form", course_sign_up: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/course_sign_ups/#{course_sign_up}")

      html = render(show_live)
      assert html =~ "Course sign up updated successfully"
      assert html =~ "some updated name"
    end
  end
end

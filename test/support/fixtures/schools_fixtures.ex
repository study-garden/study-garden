defmodule StudyGarden.SchoolsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `StudyGarden.Schools` context.
  """

  @doc """
  Generate a school.
  """
  def school_fixture(attrs \\ %{}) do
    {:ok, school} =
      attrs
      |> Enum.into(%{
        description: "some school description",
        full_name: "some full_name",
        google_calendar_embed_url: "some google_calendar_embed_url",
        short_name: "some short_name",
        stripe_customer_portal_url: "some stripe_customer_portal_url",
        timezone: "some timezone"
      })
      |> StudyGarden.Schools.create_school()

    school
  end

  @doc """
  Generate a announcement.
  """
  def announcement_fixture(attrs \\ %{}) do
    {:ok, announcement} =
      attrs
      |> Enum.into(%{
        message: "some message",
        pinned: true
      })
      |> StudyGarden.Schools.create_announcement()

    announcement
  end
end

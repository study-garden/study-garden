defmodule StudyGarden.CoursesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `StudyGarden.Courses` context.
  """

  @doc """
  Generate a unique course slug.
  """
  def unique_course_slug, do: "some slug#{System.unique_integer([:positive])}"

  @doc """
  Generate a course.
  """
  def course_fixture(attrs \\ %{}) do
    {:ok, course} =
      attrs
      |> Enum.into(%{
        deposit: "120.5",
        description: "some description",
        end_date: ~D[2024-04-02],
        slug: unique_course_slug(),
        start_date: ~D[2024-04-02],
        subtitle: "some subtitle",
        title: "some title",
        tuition: "120.5"
      })
      |> StudyGarden.Courses.create_course()

    {:ok, _} =
      attrs
      |> Enum.into(%{
        end: ~U[2024-04-21 08:35:00Z],
        start: ~U[2024-04-21 08:35:00Z],
        zoom_url: "some zoom_url",
        course_id: course.id
      })
      |> StudyGarden.Courses.create_course_session()

    StudyGarden.Courses.get_course!(course.id)
  end

  @doc """
  Generate a course_session.
  """
  def course_session_fixture(attrs \\ %{}) do
    course = course_fixture()

    {:ok, course_session} =
      attrs
      |> Enum.into(%{
        end: ~U[2024-04-21 08:35:00Z],
        start: ~U[2024-04-21 08:35:00Z],
        zoom_url: "some zoom_url",
        course_id: course.id
      })
      |> StudyGarden.Courses.create_course_session()

    course_session
  end

  @doc """
  Generate a course_sign_up.
  """
  def course_sign_up_fixture(attrs \\ %{}) do
    course = course_fixture()

    {:ok, course_sign_up} =
      attrs
      |> Enum.into(%{
        discord_username: "some discord_username",
        email: "some email",
        name: "some name",
        payment_option: :full,
        course_id: course.id
      })
      |> StudyGarden.Courses.create_course_sign_up()

    course_sign_up
  end
end

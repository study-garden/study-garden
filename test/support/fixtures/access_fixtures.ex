defmodule StudyGarden.AccessFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `StudyGarden.Access` context.
  """

  import StudyGarden.IdentitiesFixtures

  @doc """
  Generate a unique role name.
  """
  def unique_role_name, do: "some name#{System.unique_integer([:positive])}"

  @doc """
  Generate a role.
  """
  def role_fixture(attrs \\ %{}) do
    {:ok, role} =
      attrs
      |> Enum.into(%{
        name: unique_role_name()
      })
      |> StudyGarden.Access.create_role()

    role
  end

  @doc """
  Generate a user_role.
  """
  def user_role_fixture(attrs \\ %{}) do
    user = user_fixture()
    role = role_fixture()

    {:ok, user_role} =
      attrs
      |> Enum.into(%{
        user_id: user.id,
        role_id: role.id
      })
      |> StudyGarden.Access.create_user_role()

    user_role
  end
end

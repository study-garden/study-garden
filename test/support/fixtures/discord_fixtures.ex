defmodule StudyGarden.DiscordFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `StudyGarden.Discord` context.
  """

  @doc """
  Generate a unique guild_role discord_id.
  """
  def unique_guild_role_discord_id, do: "some discord_id#{System.unique_integer([:positive])}"

  @doc """
  Generate a guild_role.
  """
  def guild_role_fixture(attrs \\ %{}) do
    {:ok, guild_role} =
      attrs
      |> Enum.into(%{
        discord_color: 42,
        discord_id: unique_guild_role_discord_id(),
        discord_managed: true,
        discord_name: "some discord_name",
        discord_position: 42
      })
      |> StudyGarden.Discord.create_guild_role()

    guild_role
  end
end

defmodule StudyGarden.StripeFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `StudyGarden.Stripe` context.
  """

  @doc """
  Generate a unique stripe_id.
  """
  def unique_stripe_id, do: "some_stripe_id#{System.unique_integer([:positive])}"

  @doc """
  Generate a stripe_webhook_event.
  """
  def stripe_webhook_event_fixture(attrs \\ %{}) do
    {:ok, stripe_webhook_event} =
      attrs
      |> Enum.into(%{
        host: "some host",
        stripe_api_version: "some stripe_api_version",
        stripe_created: 42,
        stripe_id: "some stripe_id",
        stripe_request_id: "req_somestriperequestid",
        stripe_idempotency_key: "some stripe_idempotency_key",
        stripe_signature: "some stripe_signature",
        stripe_type: "some stripe_type"
      })
      |> StudyGarden.Stripe.create_stripe_webhook_event()

    stripe_webhook_event
  end


  @doc """
  Generate a stripe_product.
  """
  def stripe_product_fixture(attrs \\ %{}) do
    {:ok, stripe_product} =
      attrs
      |> Enum.into(%{
        default_price: "some default_price",
        stripe_active: true,
        stripe_created: 42,
        stripe_id: unique_stripe_id(),
        stripe_request_id: "req_somestriperequestid",
        stripe_metadata: %{},
        stripe_name: "some stripe_name",
        stripe_updated: 42,
        stripe_url: "some stripe_url"
      })
      |> StudyGarden.Stripe.create_stripe_product()

    stripe_product
  end

  @doc """
  Generate a stripe_customer.
  """
  def stripe_customer_fixture(attrs \\ %{}) do
    {:ok, stripe_customer} =
      attrs
      |> Enum.into(%{
        stripe_id: unique_stripe_id()
      })
      |> StudyGarden.Stripe.create_stripe_customer()

    stripe_customer
  end

  @doc """
  Generate a unique stripe_subscription stripe_id.
  """
  def unique_stripe_subscription_stripe_id, do: "some stripe_id#{System.unique_integer([:positive])}"

  @doc """
  Generate a stripe_subscription.
  """
  def stripe_subscription_fixture(attrs \\ %{}) do
    {:ok, stripe_subscription} =
      attrs
      |> Enum.into(%{
        sg_stripe_key: "some sg_stripe_key",
        stripe_cancel_at: 42,
        stripe_cancel_at_period_end: true,
        stripe_canceled_at: 42,
        stripe_created: 42,
        stripe_current_period_end: 42,
        stripe_current_period_start: 42,
        stripe_customer: "some stripe_customer",
        stripe_days_until_due: 42,
        stripe_ended_at: 42,
        stripe_id: unique_stripe_subscription_stripe_id(),
        stripe_items: %{},
        stripe_latest_invoice: "some stripe_latest_invoice",
        stripe_metadata: %{},
        stripe_start_date: 42,
        stripe_status: :incomplete
      })
      |> StudyGarden.Stripe.create_stripe_subscription()

    stripe_subscription
  end
end
